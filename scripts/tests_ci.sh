# setup environment variables
export TEST_NAMESPACES=(dev prod)
export DAGS_FOLDER=/opt/airflow/dags
export PLUGINS_FOLDER=/opt/airflow/plugins
export DATA_FOLDER=/opt/airflow/data
export AIRFLOW_PATH=/opt/airflow
export DAG_FILES=$(find -type f -name "*.py" -path "./dags/**" ! -name "__init__.py")
export GOOGLE_APPLICATION_CREDENTIALS=/opt/airflow/secrets/service-account.json
# setup python environment
mkdir /opt/airflow/secrets/
echo $CI_CD_SERVICE_ACCOUNT | base64 -d > $GOOGLE_APPLICATION_CREDENTIALS
# We need to either create a virtual environment or run pip install with "--user" flag.
pip install -r requirements-dev.txt --user
# initialize airflow
airflow initdb
rm -R $DAGS_FOLDER
cp -R dags/ $DAGS_FOLDER
cp -R data/ $DATA_FOLDER
cp -R plugins/ $PLUGINS_FOLDER
# Unit tests
for namespace in ${TEST_NAMESPACES[@]}; do
    export APP_NAMESPACE=${namespace}
    echo "Testing \"${APP_NAMESPACE}\" environment"
    # Loading tests
    for dag in $DAG_FILES; do
        if [[ "${dag}" =~ "dags/" ]]; then
            echo "VALIDATING: ${dag}"
            python ${dag}
        fi
    done
    echo "DAG REPO BUILD SUCCESSFUL"
    # DAGs Structure & Custom Operator tests
    python -m unittest discover -s tests -t . -v
    if [[ "$?" != "0" ]]; then
        echo "DAG STRUCTURE & CUSTOM OPERATORS TESTS FAILED"
        exit 1
    fi
    echo "DAG STRUCTURE & CUSTOM OPERATORS TESTS PASSED"
done
