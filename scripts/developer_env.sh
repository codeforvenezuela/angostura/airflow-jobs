#!/usr/bin/env bash

virtualenv -p python .venv

source .venv/bin/activate

pip install -r requirements-dev.txt

# Setup Git hooks.
pre-commit install


echo "Your virtual environment should now be active!"
echo "To deactivate run: `deactivate`"
