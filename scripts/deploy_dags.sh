#!/usr/bin/env bash
#this script is meant to replace upload_dags.sh
set -exo pipefail

##### Functions
usage()
{
    echo """
Use deploy_dags.sh to import your local changes to Cloud Composer (CC).

Usage:

./scripts/deploy_dags.sh [env] [--with_connections]

Arguments:
    - env:                  Either 'dev' or 'prod'
    - --with_connections:   If present, also configures CC connections

Prerequisites:
    * Make sure you have gcloud cli installed, on PATH, and configured with:
        - gcloud auth login
        - gcloud config set project event-pipeline
    * Your email should have permissions to import/create/delete on CC

Limitations & caveats:
    *  This script can't be used to deploy to 'prod' outside GitLab
"""
}

import_dags()
{
    echo "### Importing Dags to ${CC_ENV} ###"

    #DELETE FOLDERS IN THE DEV ENV
    eval gcloud composer environments storage dags delete --environment=${CC_ENV} --location=us-central1 --quiet
    eval gcloud composer environments storage plugins delete --environment=${CC_ENV} --location=us-central1 --quiet
    eval gcloud composer environments storage data delete config --environment=${CC_ENV} --location=us-central1 --quiet

    #UPLOAD NEW FILES
        #Each line creates a sub-process in which the working directory is temporarily set to a target folder. Example: (cd desired_temp_working_directory; command to execute in the temp working directory).
        #All files and directories inside (indicated by *) are looped thorugh and uploaded one by one to avoid unnecesary folders from being created in the dev env. Upon completion, the sub-process is terminated.
    ( cd ./data; for FILE in *; do eval gcloud composer environments storage data import --source=$FILE --environment=${CC_ENV} --location=us-central1; done;)
    ( cd ./plugins; for FILE in *; do eval gcloud composer environments storage plugins import --source=$FILE --environment=${CC_ENV} --location=us-central1; done;)
    ( cd ./dags; for FILE in *; do eval gcloud composer environments storage dags import --source=$FILE --environment=${CC_ENV} --location=us-central1 ; done;)

    echo "### [DONE] Importing Dags to ${CC_ENV} ###"
}

import_connections()
{
    echo "### Importing connections to ${CC_ENV} ###"

    #SETUP GCP_PRODUCTION CONECTION
    cronjob_sa=$(gcloud secrets versions access 1 --secret="cronjob-sa")
    echo ${cronjob_sa} | base64 --decode > cronjob-sa.json
    gcloud composer environments storage data import --source=cronjob-sa.json --environment=${CC_ENV} --location=us-central1 --destination=secrets/
    gcloud composer environments run ${CC_ENV} --project event-pipeline --location us-central1 connections -- --add --conn_id=gcp_production --conn_type=google_cloud_platform --conn_extra='{"extra__google_cloud_platform__project": "event-pipeline", "extra__google_cloud_platform__key_path": "/home/airflow/gcs/data/secrets/cronjob-sa.json", "extra__google_cloud_platform__scope": "https://www.googleapis.com/auth/cloud-platform , https://www.googleapis.com/auth/drive.readonly , https://www.googleapis.com/auth/drive  , https://www.googleapis.com/auth/bigquery"}'
    rm cronjob-sa.json

    #SETUP SLACK_ALERT_BOT CONECTION
    env=$1
    slack_pass=$(gcloud secrets versions access 1 --secret="slack_pass_${env}")
    gcloud composer environments run ${CC_ENV} --project event-pipeline --location us-central1 connections -- --add --conn_id=slack_alert_bot --conn_type=http --conn_host=https://hooks.slack.com/services --conn_password=${slack_pass}
}

if [ "$1" == "prod" ]; then
    if [ -z "$CI_CD_SERVICE_ACCOUNT" ]; then
        echo "ERROR!"
        echo "This script can't be used to deploy to 'prod' outside GitLab."
        echo "Check the DEVELOPERS.md guide for more on deploying to prod."
        exit 1
    fi
    CC_ENV=angostura-prod-cc-env
elif [ "$1" == "dev" ]; then
    CC_ENV=angostura-dev-cc-env
else
    usage
    exit 1
fi
if [ "$2" == "--with_connections" ]; then
    import_connections "$1"
fi
import_dags
