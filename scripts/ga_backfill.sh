#!/usr/bin/env bash
set -eo pipefail

#Point of contact: Nicolás Silveira - nicolas.silveira@codeforvenezuela.org

DIFF=$(( ($(date -d $3 +%s) - $(date -d $2 +%s)) / 86400 ))
EXE_START=`date +%s`
MAX_RANGE="7"

usage()
{
    echo """
    Use ./scripts/ga_backfill.sh to execute a backfill for a specified DAG and date range.

    Usage:

    ./scripts/ga_backfill.sh [env] [start date] [end date]

    Arguments:
        -env:           Either 'dev or prod'
        -start date:    YYYY-MM-DD (inclusive)
        -end date:      YYYY-MM-DD (exclusive)

    Prerequisites:
        *Make sure you have gcloud installed and configured with:
            -gcloud auth login
            -gcloud config set project event-pipeline
        * Your email should have permissions to work with the CC environments

    Limitations & caveats:
        *The date range can't be over 7 days at a time to avoid overloading the composer env
    """
}

execute_backfill()
{
    #The GA to GCS backfill is executed fully before the GCS to BQ backfill is executed

    echo "Executing GA to GCS backfill..."

    gcloud composer environments run angostura-${CC_ENV}-cc-env \
    --location us-central1 \
    backfill -- \
    -s ${START_DATE} -e ${END_DATE} \
    ga_to_gcs

    echo "Executing GCS to BQ backfill..."

    gcloud composer environments run angostura-${CC_ENV}-cc-env \
    --location us-central1 \
    backfill -- \
    -s ${START_DATE} -e ${END_DATE} \
    curitas_gcs_to_bq_daily_2AM_UTC

    echo "Backfill done!"
}

if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ]; then
    #Avoid execution if not all arguments are provided
    usage
    exit 1
elif [[ $DIFF -gt $MAX_RANGE ]]; then
    #Avoid execution if date range is over 7 days
    usage
    exit 1
else
    #Allow execution if all arguments are provided
    #If any argument is invalid, GCloud will handle those errors by itself
    CC_ENV=$1
    START_DATE=$2
    END_DATE=$3

    execute_backfill
fi

EXE_END=`date +%s`
RUNTIME=$((EXE_END-EXE_START))
echo "Execution time: $RUNTIME seconds"
