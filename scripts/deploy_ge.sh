#!/usr/bin/env bash

# This script is for uploading our Great Expectations configuration to its respective bucket.
# The script also updates the configuration stored in our Cloud Composer environment.

# Functions
usage()
{
    echo """
Use deploy_ge.sh to deploy your local changes to Great Expectations Bucket and Cloud Composer.
Usage:
    ./scripts/deploy_ge.sh dev

Prerequisites:
    * Make sure you have gsutil and gcloud cli installed, on PATH, and configured with:
        - gcloud auth login.
        - gcloud config set project event-pipeline.
    * Your email should have permissions to import/create/delete on Cloud Composer.

Caveats:
    * using this script with the 'prod' argument is not for development.
"""
}

deploy_great_expectations()
{
    echo "# Deploying Great Expectations to ${GE_BUCKET} #"
    gsutil -m cp -r great_expectations/checkpoints/* gs://${GE_BUCKET}/checkpoints
    gsutil -m cp -r great_expectations/expectations/* gs://${GE_BUCKET}/expectations
    gsutil -m cp -r great_expectations/notebooks/* gs://${GE_BUCKET}/notebooks
    gsutil -m cp -r great_expectations/plugins/* gs://${GE_BUCKET}/plugins
    gsutil -m cp -r great_expectations/validations/* gs://${GE_BUCKET}/validations
    echo "# Deployment ready!"

    echo "# Updating Cloud Composer ${CC_ENV} environment #"
    gcloud composer environments storage data delete great_expectations.yml --environment=${CC_ENV} --location=us-central1 --quiet
    gcloud composer environments storage data import --source=great_expectations/great_expectations.yml --environment=${CC_ENV} --location=us-central1
    echo "# Update ready!"
}

env=$1
if [ $env == prod ]; then
    CC_ENV=angostura-prod-cc-env
    GE_BUCKET=angostura_great_expectations
elif [ $env == dev ]; then
    CC_ENV=angostura-dev-cc-env
    GE_BUCKET=angostura_great_expectations_dev
else
    usage
    exit 0
fi
deploy_great_expectations
