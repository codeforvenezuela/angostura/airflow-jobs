# Airflow Jobs

[![pylint](https://codeforvenezuela.gitlab.io/angostura/airflow-jobs/badges/pylint.svg)](https://codeforvenezuela.gitlab.io/angostura/airflow-jobs/lint/)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

Repository containing Airflow DAGs definition for Angostura jobs

## Develop
We've set up a separate document for [developers](DEVELOPERS.md).

## Guides

* If you want to add another table, read [Add a new BigQuery Table](https://www.notion.so/Add-a-new-BigQuery-table-f3c1f072476b4f4187ed376fcf30fc43)

## License

This project is licensed under the MIT License - see the
[LICENSE.md](LICENSE.md) file for details
