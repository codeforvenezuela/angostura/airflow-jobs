"""Base tests needed for every DAG"""
from collections import namedtuple
from datetime import datetime
from unittest import TestCase
from airflow.models import DAG
from plugins.common import get_dag_default_args


DagInfo = namedtuple("Dag", "dag_id tasks_dependencies")

# General tests for all dags


def test_dags_loaded(dag_test_instance: TestCase) -> None:
    """Tests if dags load without errors and with their respective number of tasks"""
    dag_test_instance.assertDictEqual(dag_test_instance.dagbag.import_errors, {})
    for dag in dag_test_instance.dags:
        loaded_dag = dag_test_instance.dagbag.get_dag(dag_id=dag.dag_id)
        dag_test_instance.assertIsNotNone(loaded_dag)
        dag_test_instance.assertEqual(len(loaded_dag.tasks), len(dag.tasks_dependencies.keys()))


def test_dags_structure(dag_test_instance: TestCase) -> None:
    """Checks that tasks follow the defined dependencies structure"""
    for dag in dag_test_instance.dags:
        loaded_dag = dag_test_instance.dagbag.get_dag(dag_id=dag.dag_id)
        dag_test_instance.assertEqual(loaded_dag.task_dict.keys(), dag.tasks_dependencies.keys())
        for task_id, upstream_list in dag.tasks_dependencies.items():
            dag_test_instance.assertTrue(
                loaded_dag.has_task(task_id),
                msg=f"Missing task_id: {task_id} in dag",
            )
            task = loaded_dag.get_task(task_id)
            dag_test_instance.assertEqual(
                task.upstream_task_ids,
                set(upstream_list),
                msg=f"unexpected upstream link in {task_id}",
            )


# Operators Tests Helpers

DEFAULT_DATE = datetime(2021, 5, 26)


def build_example_dag(dag_id: str) -> DAG:
    """Build an example dag for Operator tests"""
    return DAG(
        dag_id=dag_id,
        schedule_interval="@daily",
        default_args={**get_dag_default_args(), "start_date": DEFAULT_DATE},
    )
