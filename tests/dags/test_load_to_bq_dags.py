"""
# Load to Bigquery DAGs Unit Tests

# Contributors

- Alvaro Avila (aavila@codeforvenezuela.org)

"""
from unittest import TestCase
from airflow.models import DagBag
from tests.utils import DagInfo, test_dags_loaded, test_dags_structure
from plugins.common import load_config


# Helper function
def get_task_id_by_task_type(task_type, suffix):
    """
    Returns the task_id according to the task_type.
    This follows the structure defined in load_to_bq_dags.py
    """
    task_id_to_type_mapping = {
        "gcs_sensor": "gcs_sensor_",
        "bq_drop": "bq_drop_",
        "gcs_to_bq": "gcs_to_bq_",
        "spreadsheet_to_bq": "create_external_table_",
        "sql_transform": "sql_transform_",
        "bq_to_hdx": "bq_to_hdx_",
        "python_transform": "python_transform_",
    }
    return task_id_to_type_mapping.get(task_type, "") + suffix


def find_task_by_job_id(tasks_list, job_id):
    """Searches for a task that has the job_id in the tasks_list"""
    for task in tasks_list:
        if task["job_id"] == job_id:
            return task
    return None


def build_dags():
    """Build dags structure for Load to Bigquery tests"""
    dags = []
    # Add dags from config files
    config = load_config()
    for dag_config in config["load_to_bq_dags"]:
        # Build dag structure
        dag_tasks_dependencies = {}
        for task in dag_config["jobs"]:
            task_id = get_task_id_by_task_type(task["job_type"], task["job_id"])
            task_dependencies = []
            for job_id in task.get("dependencies", []):
                dependency_task = find_task_by_job_id(dag_config["jobs"], job_id)
                task_dependencies.append(
                    get_task_id_by_task_type(
                        dependency_task["job_type"],
                        job_id,
                    )
                )
            dag_tasks_dependencies[task_id] = task_dependencies
        dags.append(DagInfo(dag_config["name"], dag_tasks_dependencies))
    return dags


class LoadToBigqueryUnitTests(TestCase):
    """Unit tests for Load to Bigquery DAGs"""

    @classmethod
    def setUpClass(cls):
        cls.dagbag = DagBag()
        cls.dags = build_dags()

    def test_dags_loaded(self):
        """Tests if dags load without errors and with their respective number of tasks"""
        test_dags_loaded(self)

    def test_dags_structure(self):
        """Checks that tasks follow the defined dependencies structure"""
        test_dags_structure(self)
