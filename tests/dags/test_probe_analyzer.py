"""
# Probe Analyzer DAG Unit Tests

# Contributors

- Alvaro Avila (aavila@codeforvenezuela.org)

"""
from unittest import TestCase
from airflow.models import DagBag
from tests.utils import DagInfo, test_dags_loaded, test_dags_structure


def build_dags():
    """Build dag structure for probe analyzer tests"""
    probe_analyzer_tasks_dependencies = {"analyze_probe": []}
    return [DagInfo("probe_analyzer", probe_analyzer_tasks_dependencies)]


class ProbeAnalyzerUnitTests(TestCase):
    """Unit tests for Load to Probe Analyzer DAG"""

    @classmethod
    def setUpClass(cls):
        cls.dagbag = DagBag()
        cls.dags = build_dags()

    def test_dags_loaded(self):
        """Tests if dags load without errors and with their respective number of tasks"""
        test_dags_loaded(self)

    def test_dags_structure(self):
        """Checks that tasks follow the defined dependencies structure"""
        test_dags_structure(self)
