"""
# Pubsub to Bigquery DAG Unit Tests

# Contributors

- Alvaro Avila (aavila@codeforvenezuela.org)

"""
from unittest import TestCase
from airflow.models import DagBag
from tests.utils import DagInfo, test_dags_loaded, test_dags_structure
from plugins.common import load_config


def build_dags():
    """Build dags for pubsub to bigquery tests"""
    # Add dag info from config files
    config = load_config()
    # Build tasks dependencies structure
    pubsub_to_bq_dependencies = {}
    for job_config in config["pubsub_to_bq_jobs"]:
        suffix = job_config["name"]
        pubsub_task_id = f"pubsub_to_gcs_dataflow_{suffix}"
        drain_task_id = f"pubsub_dataflow_drain_{suffix}"
        fail_drain_task_id = f"pubsub_fail_dataflow_drain_{suffix}"
        pubsub_to_bq_dependencies[pubsub_task_id] = []
        pubsub_to_bq_dependencies[drain_task_id] = [pubsub_task_id]
        pubsub_to_bq_dependencies[fail_drain_task_id] = [pubsub_task_id]

        bq_load_config = job_config["bq_load_job"]
        suffix = bq_load_config["job_name_suffix"]
        bq_temp_load_task_id = f"gcs_to_bq_dataflow_{suffix}"
        pubsub_to_bq_dependencies[bq_temp_load_task_id] = [drain_task_id]

        bq_load_task_id = "bq_temp_to_bq"
        pubsub_to_bq_dependencies[bq_load_task_id] = [bq_temp_load_task_id]

        bq_tranform_config = bq_load_config.get("post_transform", [])
        for job in bq_tranform_config["jobs"]:
            suffix = job["job_name_suffix"]
            sql_transform_task_id = f"transform_sql_{suffix}"
            pubsub_to_bq_dependencies[sql_transform_task_id] = [bq_load_task_id]
    return [DagInfo("pubsub_to_bigquery", pubsub_to_bq_dependencies)]


class PubsubToBigqueryUnitTests(TestCase):
    """Unit tests for Load to Pubsub to Bigquery DAG"""

    @classmethod
    def setUpClass(cls):
        cls.dagbag = DagBag()
        cls.dags = build_dags()

    def test_dags_loaded(self):
        """Tests if dags load without errors and with their respective number of tasks"""
        test_dags_loaded(self)

    def test_dags_structure(self):
        """Checks that tasks follow the defined dependencies structure"""
        test_dags_structure(self)
