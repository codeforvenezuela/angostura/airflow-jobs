"""
# common.py Unit Tests

# Contributors

- Alvaro Avila (aavila@codeforvenezuela.org)

"""
import sys
from unittest import TestCase
from plugins.common import bulk_sql_values_param


class BulkSqlValuesParamTest(TestCase):
    "bulk_sql_values_param Unit Tests"

    def test_short_values(self):
        """
        Test the function for param values less than 2**16 bytes long.
        In the assertion we check that the dict key matches to the param_key defined.
        We also check that the formatting works as expected by comparing the value of the dict.
        Finally, we ensure that we are retrieving a single dictionary in the list of query_params.
        """
        param_key = "value"
        params = ["value_1", "value_2", "value_3"]
        query_params = bulk_sql_values_param(param_key=param_key, params=params, format_fn=str)
        expected_output = [{"value": "(value_1),(value_2),(value_3)"}]
        self.assertListEqual(query_params, expected_output)

    def test_long_values(self):
        """
        Test the function for param values longer than 2**16 bytes long
        The function should split the list in multiple dicts for different queries.
        Each value should not be greater than the limit 2 ** 17 bytes for every key.
        Each value (except the last one) should surpass the threshold of 2 ** 16 bytes.
        """
        param_key = "value"
        params = [f"value_{i}" for i in range(0, 2 ** 16)]
        query_params = bulk_sql_values_param(param_key=param_key, params=params, format_fn=str)
        self.assertGreater(len(query_params), 1)
        # Each element should not be greater than the limit 2 ** 17 bytes
        for param in query_params:
            self.assertLess(sys.getsizeof(param[param_key]), 2 ** 17)
        # Each element (except the last one) should surpass the threshold of 2 ** 16 bytes
        for param in query_params[:-1]:
            self.assertGreater(sys.getsizeof(param[param_key]), 2 ** 16)
