"""
# Tests for Bigquery Googlesheet Operator

follows the best practices for operator tests defined in
https://airflow.apache.org/docs/apache-airflow/1.10.11/best-practices.html#unit-tests

# Contributors

- Alvaro Avila (aavila@codeforvenezuela.org)

"""
import json
from unittest import TestCase
from unittest.mock import MagicMock, patch
from airflow.models import TaskInstance
from airflow.utils.state import State
from plugins.operators.bigquery_google_sheet_operator import (
    BigQueryCreateGoogleSheetTableOperator,
)
from plugins.common import read_local_file
from tests.utils import build_example_dag, DEFAULT_DATE


TEST_DAG_ID = "test_bigquery_google_sheet_operator"
TABLE_ID = "event-pipeline.angostura_dev.covid_hospital_ira_response_raw_test"
GCP_CONN_ID = "GCP_CONN_ID_TEST"


class BigqueryGooglesheetOperatorTest(TestCase):
    """Unit tests for Bigquery Google Sheet Operator"""

    def setUp(self):
        # Build example dag to use the operator.
        self.dag = build_example_dag(TEST_DAG_ID)

        self.schema_fields = json.loads(
            read_local_file("bq_schemas/covid_hospital_ira_response.json")
        )
        self.source_uris = [
            "https://docs.google.com/spreadsheets/d/1nIagXJ-FUT_paAgi6-dHrsOUj5xx0CxLb7uAThb0Kso/edit#gid=1003838168",
        ]
        self.operator = BigQueryCreateGoogleSheetTableOperator(
            dag=self.dag,
            task_id="create_external_table_test",
            ignore_unknown_values=False,
            autodetect=False,
            destination_project_dataset_table=TABLE_ID,
            source_uris=self.source_uris,
            schema_fields=self.schema_fields,
            skip_leading_rows=1,
            max_bad_records=2000,
            quote_character=None,
            allow_quoted_newlines=False,
            allow_jagged_rows=False,
            bigquery_conn_id=GCP_CONN_ID,
        )
        self.task_instance = TaskInstance(task=self.operator, execution_date=DEFAULT_DATE)

    @patch("plugins.operators.bigquery_google_sheet_operator.BigQueryHook")
    def test_execution_mock(self, mock_bq_hook):
        """Test that the task follows the expected normal workflow"""
        # Build the mocked structure
        mock_bq_cursor = MagicMock()
        mock_bq_cursor.create_external_table = MagicMock()
        mock_bq_connection = MagicMock()
        mock_bq_connection.cursor = MagicMock(return_value=mock_bq_cursor)
        mock_bq_hook.get_conn = MagicMock(return_value=mock_bq_connection)
        mock_bq_hook.return_value = mock_bq_hook
        # Call to task execution
        self.task_instance.run(ignore_ti_state=True)
        self.assertEqual(self.task_instance.state, State.SUCCESS)
        # Assertions related to task workflow
        mock_bq_hook.assert_called_once_with(
            bigquery_conn_id=GCP_CONN_ID,
            delegate_to=None,
        )

        mock_bq_hook.get_conn.assert_called_once()
        mock_bq_connection.cursor.assert_called_once()
        mock_bq_cursor.create_external_table.assert_called_once_with(
            external_project_dataset_table=TABLE_ID,
            schema_fields=self.schema_fields,
            source_uris=self.source_uris,
            source_format="GOOGLE_SHEETS",
            compression="NONE",
            skip_leading_rows=1,
            field_delimiter=",",
            max_bad_records=2000,
            quote_character=None,
            allow_quoted_newlines=False,
            allow_jagged_rows=False,
            src_fmt_configs={},
            labels=None,
            encryption_configuration=None,
            ignore_unknown_values=False,
            autodetect=False,
        )
