"""
# Tests for Bigquery To HDX Operator

follows the best practices for operator tests defined in
https://airflow.apache.org/docs/apache-airflow/1.10.11/best-practices.html#unit-tests

# Contributors

- Alvaro Avila (aavila@codeforvenezuela.org)

"""
from unittest import TestCase
from unittest.mock import MagicMock, patch, call
from airflow.models import TaskInstance
from airflow.utils.state import State
from hdx.data.hdxobject import HDXError
from plugins.operators.bigquery_to_hdx_operator import (
    BigQueryToHdxOperator,
)
from plugins.common import read_local_file
from tests.utils import build_example_dag, DEFAULT_DATE


TEST_DAG_ID = "test_bigquery_to_hdx_operator"
GCP_CONN_ID = "GCP_CONN_ID_TEST"


@patch("plugins.operators.bigquery_to_hdx_operator.Resource")
@patch("plugins.operators.bigquery_to_hdx_operator.BigQueryHook")
@patch("plugins.operators.bigquery_to_hdx_operator.Dataset")
class BigqueryToHDXOperatorTest(TestCase):
    """Unit tests for Bigquery to HDX Operator"""

    def setUp(self):
        # Build example dag to use the operator.
        self.dag = build_example_dag(TEST_DAG_ID)
        self.hdx_resources = [
            {
                "format": "CSV",
                "description": "Csv probe data",
            },
            {
                "format": "JSON",
                "description": "JSON Probe data",
            },
        ]
        self.operator = BigQueryToHdxOperator(
            dag=self.dag,
            task_id="bq_to_hdx_test",
            bigquery_conn_id=GCP_CONN_ID,
            hdx_dataset_name="probe",
            hdx_dataset_title="Probe Angostura",
            hdx_dataset_private=True,
            hdx_dataset_methodology="Registry",
            hdx_dataset_update_frequency="Every day",
            hdx_dataset_start_date="2020-01-01",
            hdx_dataset_end_date=None,
            hdx_dataset_ongoing=True,
            hdx_dataset_locations=["VEN"],
            maintainer_id="e44ffe57-11b5-4a2f-9f8d-8bee9c104af1",
            organization_id="656bb93a-3982-4308-8243-511e058c33ed",
            hdx_resources=self.hdx_resources,
            hdx_config={
                "dataset": {
                    "required_fields": ["name", "title", "dataset_date"],
                },
                "resource": {
                    "required_fields": ["package_id", "name", "description"],
                },
                "showcase": {
                    "required_fields": ["name", "title"],
                },
            },
            sql="plugins/bq_sql/utils/select_star.sql",
            params={"table_name": "angostura_dev.probe"},
            test_mode=True,
        )
        self.task_instance = TaskInstance(task=self.operator, execution_date=DEFAULT_DATE)

    def test_execution_mock(self, mock_dataset, mock_bq_hook, mock_resource):
        """Test that the task follows the expected normal workflow"""
        # Build the mocked structure
        mock_bq_result = MagicMock()
        mock_bq_hook.get_pandas_df = MagicMock(return_value=mock_bq_result)
        mock_bq_hook.return_value = mock_bq_hook
        mock_dset_obj = MagicMock()
        mock_dataset.return_value = mock_dset_obj
        mock_resource_obj = MagicMock()
        mock_resource.return_value = mock_resource_obj
        # Call to task execution
        self.task_instance.run(ignore_ti_state=True)
        self.assertEqual(self.task_instance.state, State.SUCCESS)
        # Assertions related to task workflow
        mock_bq_hook.assert_called_once_with(
            bigquery_conn_id=GCP_CONN_ID,
            use_legacy_sql=False,
            delegate_to=None,
        )
        sql_file = read_local_file("bq_sql/utils/select_star.sql").strip()
        mock_bq_hook.get_pandas_df.assert_called_once_with(
            sql_file.replace("{{ params.table_name }}", "angostura_dev.probe")
        )
        dataset_dict = {
            "name": "probe",
            "private": True,
            "title": "Probe Angostura",
            "methodology": "Registry",
            "license_id": "ODC-ODbL",
            "dataset_source": "Angostura",
        }
        mock_dataset.assert_called_once_with(dataset_dict)
        mock_dset_obj.set_date_of_dataset.assert_called_once_with("2020-01-01", None, True)
        mock_dset_obj.set_maintainer.assert_called_once_with("e44ffe57-11b5-4a2f-9f8d-8bee9c104af1")
        mock_dset_obj.set_organization.assert_called_once_with(
            "656bb93a-3982-4308-8243-511e058c33ed"
        )
        mock_dset_obj.add_country_locations.assert_called_once_with(["VEN"])
        mock_dset_obj.set_expected_update_frequency.assert_called_once_with("Every day")
        for resource in self.hdx_resources:
            res_format = resource["format"]
            res_config = {
                "name": f"probe.{res_format.lower()}",
                "description": resource["description"],
                "format": res_format,
            }
            self.assertIn(call(res_config), mock_resource.mock_calls)
        mock_resource_obj.set_file_to_upload.assert_called()
        mock_bq_result.to_csv.assert_called_once()
        mock_bq_result.to_json.assert_called_once()
        mock_dset_obj.add_update_resource.assert_called_with(mock_resource_obj)
        mock_dset_obj.update_in_hdx.assert_called_once()
        mock_dset_obj.create_in_hdx.assert_not_called()

    # Even tho the arguments are not explicitly used inside the tests, the mocked arguments are
    # required to complete the workflow without trying to connect to Bigquery or HDX.
    # pylint: disable=unused-argument
    def test_execution_without_dataset(self, mock_dataset, mock_bq_hook, mock_resource):
        """Tests execution without an HDX dataset created"""
        # Build the mocked structure
        mock_dset_obj = MagicMock()
        mock_dset_obj.update_in_hdx = MagicMock(side_effect=HDXError)
        mock_dset_obj.__setitem__ = MagicMock()
        mock_dataset.return_value = mock_dset_obj
        # Call to task execution
        self.task_instance.run(ignore_ti_state=True)
        self.assertEqual(self.task_instance.state, State.SUCCESS)
        # Assertions related to task workflow
        mock_dset_obj.__setitem__.assert_called_once_with("notes", "TBD")
        mock_dset_obj.create_in_hdx.assert_called_once()
