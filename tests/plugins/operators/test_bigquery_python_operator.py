"""
# Tests for Bigquery Python Operator

follows the best practices for operator tests defined in
https://airflow.apache.org/docs/apache-airflow/1.10.11/best-practices.html#unit-tests

# Contributors

- Alvaro Avila (aavila@codeforvenezuela.org)

"""
from unittest import TestCase
from unittest.mock import MagicMock, patch
from airflow.models import TaskInstance
from airflow.utils.state import State
from plugins.operators.bigquery_python_operator import (
    BigQueryPythonOperator,
)
from plugins.common import read_local_file
from tests.utils import build_example_dag, DEFAULT_DATE


TEST_DAG_ID = "test_bigquery_python_operator"
GCP_CONN_ID = "GCP_CONN_ID_TEST"


class BigqueryPythonOperatorTest(TestCase):
    """Unit tests for Bigquery Python Operator"""

    def setUp(self):
        # Build example dag to use the operator.
        self.dag = build_example_dag(TEST_DAG_ID)
        self.sql_files = {"probe": "bq_sql/python_transformation_sqls/probe_dev.sql"}
        self.operator = BigQueryPythonOperator(
            dag=self.dag,
            task_id="python_transform_test",
            kwargs_mapping={key: f"plugins/{value}" for key, value in self.sql_files.items()},
            output_table="angostura_dev.python_transform_demo",
            python_function="test_function",
            bigquery_conn_id=GCP_CONN_ID,
            params={},
            delegate_to=None,
        )
        self.task_instance = TaskInstance(task=self.operator, execution_date=DEFAULT_DATE)

    @patch("plugins.operators.bigquery_python_operator.importlib")
    @patch("plugins.operators.bigquery_python_operator.BigQueryHook")
    def test_execution_mock(self, mock_bq_hook, mock_importlib):
        """Test that the task follows the expected normal workflow"""
        # Build the mocked structure
        mock_bq_hook.return_value = mock_bq_hook
        mock_bq_hook.get_pandas_df = MagicMock(return_value=None)
        # pylint: disable=protected-access
        mock_bq_hook._get_field = MagicMock(return_value="_get_field")

        mock_result = MagicMock()
        mock_result.to_gbq = MagicMock()
        mock_transformations = MagicMock()
        mock_transformations.test_function = MagicMock(return_value=mock_result)
        mock_importlib.import_module = MagicMock(return_value=mock_transformations)
        # Call to task execution
        self.task_instance.run(ignore_ti_state=True)
        self.assertEqual(self.task_instance.state, State.SUCCESS)
        # Assertions related to task workflow
        mock_bq_hook.assert_called_with(
            bigquery_conn_id=GCP_CONN_ID,
            use_legacy_sql=False,
            delegate_to=None,
        )

        for sql_file in self.sql_files.values():
            file_content = read_local_file(sql_file).strip()
            mock_bq_hook.get_pandas_df.assert_called_once_with(file_content)

        mock_importlib.import_module.assert_called_once_with("python_transformations")
        mock_transformations.test_function.assert_called_once()
        mock_result.to_gbq.assert_called_once_with(
            "angostura_dev.python_transform_demo",
            "_get_field",
            if_exists="replace",
        )
