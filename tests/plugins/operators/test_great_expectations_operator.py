"""
# Tests for Great Expectations Operator

follows the best practices for operator tests defined in
https://airflow.apache.org/docs/apache-airflow/1.10.11/best-practices.html#unit-tests

# Contributors

- Alvaro Avila (aavila@codeforvenezuela.org)

"""
import sys
from unittest import TestCase
from unittest.mock import MagicMock, patch
from airflow.models import TaskInstance
from airflow.utils.state import State

sys.path.append("./plugins")
# pylint: disable=import-error, wrong-import-position
from plugins.operators.great_expectations_operator import (
    GreatExpectationsOperator,
)
from tests.utils import build_example_dag, DEFAULT_DATE


TEST_DAG_ID = "test_great_expectations_operator"
GCP_CONN_ID = "GCP_CONN_ID_TEST"
SLACK_CONN_ID = "SLACK_CONN_ID_TEST"


class GreatExpectationsOperatorTest(TestCase):
    """Unit tests for Bigquery Google Sheet Operator"""

    def setUp(self):
        # Build example dag to use the operator.
        self.dag = build_example_dag(TEST_DAG_ID)

        self.operator = GreatExpectationsOperator(
            bigquery_conn_id=GCP_CONN_ID,
            slack_conn_id=SLACK_CONN_ID,
            checkpoint_name="checkpoint_test",
            fail_task_on_validation_failure=True,
            runtime_environment={
                "EXAMPLE_PARAM": "EXAMPLE_VALUE",
            },
            dag=self.dag,
            task_id="great_expectations_task_id",
        )
        self.task_instance = TaskInstance(task=self.operator, execution_date=DEFAULT_DATE)

    @patch("plugins.operators.great_expectations_operator.BigQueryHook")
    @patch("plugins.operators.great_expectations_operator.BaseHook")
    @patch("plugins.operators.great_expectations_operator.DataContext")
    def test_execution_mock(
        self,
        mock_data_context_cls,
        mock_basehook_cls,
        mock_bq_hook_cls,
    ):
        """Test that the task follows the expected normal workflow"""
        # Build the mocked structure
        mock_bq_hook = MagicMock(name="BigQueryHook.Instance")
        mock_bq_hook.extras = MagicMock(
            return_value={
                "extra__google_cloud_platform__key_path": "example/keypath.json",
            }
        )
        mock_bq_hook_cls.return_value = mock_bq_hook
        mock_conn = MagicMock()
        mock_conn.host = MagicMock(return_value="example_host")
        mock_conn.password = MagicMock(return_value="example_pass")
        mock_basehook_cls.get_connection = MagicMock(return_value=mock_conn)
        mock_data_context = MagicMock(name="DataContext.Instance")
        mock_data_context_cls.return_value = mock_data_context
        mock_checkpoint = MagicMock(name="Checkpoint.Instance")
        mock_data_context.get_checkpoint.return_value = mock_checkpoint
        mock_checkpoint.run.return_value = {
            "success": True,
        }
        # Call to task execution
        self.task_instance.run(ignore_ti_state=True)
        self.assertEqual(self.task_instance.state, State.SUCCESS)
        # Assertions related to task execution results
        mock_bq_hook_cls.assert_called_once_with(bigquery_conn_id=GCP_CONN_ID)
        mock_checkpoint.run.assert_called_once()
