"""
# Tests for Dataflow Drain Operator

follows the best practices for operator tests defined in
https://airflow.apache.org/docs/apache-airflow/1.10.11/best-practices.html#unit-tests

# Contributors

- Alvaro Avila (aavila@codeforvenezuela.org)

"""
from unittest import TestCase
from unittest.mock import MagicMock, patch
from airflow.models import TaskInstance
from airflow.utils.state import State
from plugins.operators.dataflow_drain_operator import (
    DataflowDrainOperator,
)
from tests.utils import build_example_dag, DEFAULT_DATE


TEST_DAG_ID = "test_dataflow_drain_operator"
GCP_CONN_ID = "GCP_CONN_ID_TEST"


class BigqueryToHDXOperatorTest(TestCase):
    """Unit tests for Dataflow Drain Operator"""

    def setUp(self):
        # Build example dag to use the operator.
        self.dag = build_example_dag(TEST_DAG_ID)
        self.operator = DataflowDrainOperator(
            dag=self.dag,
            task_id="pubsub_dataflow_drain_test",
            job_name="pubsub_to_gcs_dataflow_test",
            gcp_conn_id=GCP_CONN_ID,
            wait_time_s=0,
            dataflow_default_options={
                "project": "test_project",
                "zone": "test_zone",
            },
        )
        self.task_instance = TaskInstance(task=self.operator, execution_date=DEFAULT_DATE)

    @patch("plugins.operators.dataflow_drain_operator.DataFlowHook")
    def test_execution_mock(self, mock_df_hook):
        """Test that the task follows the expected normal workflow"""
        # Build the mocked structure
        mock_df_conn = MagicMock(name="DataFlowConnection")
        mock_df_conn.projects.return_value = mock_df_conn.projects
        mock_df_conn.projects.locations.return_value = mock_df_conn.projects.locations
        mock_df_conn.projects.locations.jobs.return_value = mock_df_conn.projects.locations.jobs
        mock_df_conn.projects.locations.jobs.list.return_value = (
            mock_df_conn.projects.locations.jobs.list
        )
        mock_df_conn.projects.locations.jobs.list.execute.return_value = {
            "jobs": [
                {
                    "id": "1234",
                    "name": "pubsub-to-gcs-dataflow-test-suffix",
                },
            ]
        }
        mock_df_hook_obj = MagicMock(name="DataFlowHook.Instance")
        mock_df_hook_obj.get_conn.return_value = mock_df_conn
        mock_df_hook.return_value = mock_df_hook_obj
        # Call to task execution
        self.task_instance.run(ignore_ti_state=True)
        self.assertEqual(self.task_instance.state, State.SUCCESS)
        # Assertions related to task execution results
        mock_df_hook.assert_called_once_with(gcp_conn_id=GCP_CONN_ID)
        mock_df_hook_obj.get_conn.assert_called_once()
        mock_df_conn.projects.assert_called()
        mock_df_conn.projects.locations.assert_called()
        mock_df_conn.projects.locations.jobs.assert_called()
        mock_df_conn.projects.locations.jobs.list.assert_called_once_with(
            projectId="test_project",
            location="test_zone",
            filter="ACTIVE",
        )
        mock_df_conn.projects.locations.jobs.update.assert_called_once_with(
            projectId="test_project",
            location="test_zone",
            jobId="1234",
            body={
                "id": "1234",
                "requestedState": "JOB_STATE_DRAINING",
            },
        )
