"""
This file defines the Airflow DAGs json schema Configuration. This is intended
to be used as a pipeline to validate that the data inside the json config files
is correct before sending it to airflow.

To do this, we use Json Schema plus our own custom validations. To learn about
this tool, visit https://json-schema.org.

To view a quick tutorial about how this tool works visit
https://json-schema.org/learn/getting-started-step-by-step.

Finally, if you want to go deeper into its validation features, visit
https://json-schema.org/draft/2020-12/json-schema-validation.html.

# How to use

```bash
config_validator.py <files_list>
```

# Contributors:

- Alvaro Avila (aavila@codeforvenezuela.org)

"""
import json
import logging
import argparse
from jsonschema import validate
from jsonschema.exceptions import ValidationError

# Our schema
_CONFIGURATION_SCHEMA = {
    "title": "Configuration Schema",
    "description": "The configuration that all dags defined in .json files should follow",
    "type": "object",
    "properties": {
        "gcp_project_id": {
            "description": "The Google Cloud Platform Project Id.",
            "type": "string",
        },
        "hdx_config": {
            "description": """
                Configuration for interacting with Humanitarian Data Exchange (HDX) API.
            """,
            "type": "object",
            "properties": {
                "organization_id": {
                    "description": "The HDX Organization Id.",
                    "type": "string",
                },
                "maintainer_id": {
                    "description": "The HDX Maintainer Id from the Organization.",
                    "type": "string",
                },
                "project_configuration": {
                    "description": "The HDX Project Configuration",
                    "type": "object",
                    "properties": {
                        "dataset": {
                            "description": "The dataset required fields.",
                            "type": "object",
                            "properties": {
                                "required_fields": {
                                    "description": "The specific required fields",
                                    "type": "array",
                                    "items": {"type": "string"},
                                    "uniqueItems": True,
                                }
                            },
                            "additionalProperties": False,
                            "required": ["required_fields"],
                        },
                        "resource": {
                            "description": "The resource required fields.",
                            "type": "object",
                            "properties": {
                                "required_fields": {
                                    "description": "The specific required fields",
                                    "type": "array",
                                    "items": {"type": "string"},
                                    "uniqueItems": True,
                                }
                            },
                            "additionalProperties": False,
                            "required": ["required_fields"],
                        },
                        "showcase": {
                            "description": "The showcase required fields.",
                            "type": "object",
                            "properties": {
                                "required_fields": {
                                    "description": "The specific required fields",
                                    "type": "array",
                                    "items": {"type": "string"},
                                    "uniqueItems": True,
                                }
                            },
                            "additionalProperties": False,
                            "required": ["required_fields"],
                        },
                    },
                    "additionalProperties": False,
                    "required": ["dataset", "resource", "showcase"],
                },
            },
            "additionalProperties": False,
            "required": ["organization_id", "maintainer_id", "project_configuration"],
        },
        "pubsub_to_bq_jobs": {
            "description": "Jobs that send data from Pubsub to Bigquery.",
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "name": {
                        "description": "The Job name",
                        "type": "string",
                    },
                    "pubsub_subscription": {
                        "description": "Pubsub Subscription Details",
                        "type": "object",
                        "properties": {
                            "name": {
                                "description": "Pubsub subscription name",
                                "type": "string",
                            },
                            "subscription": {
                                "description": "Pubsub subscription identifier",
                                "type": "string",
                            },
                            "gcs_output_directory": {
                                "description": "GCS output directory",
                                "type": "string",
                                "pattern": "gs://[^/]+(/[^/]+)*/$",
                            },
                            "output_filename_prefix": {
                                "description": "Prefix for output files",
                                "type": "string",
                            },
                        },
                        "additionalProperties": False,
                        "required": [
                            "name",
                            "subscription",
                            "gcs_output_directory",
                            "output_filename_prefix",
                        ],
                    },
                    "bq_load_job": {
                        "description": "Bigquery Load Job",
                        "type": "object",
                        "properties": {
                            "job_name_suffix": {
                                "description": "Job name suffix",
                                "type": "string",
                            },
                            "temp_table_name": {
                                "description": "The temporary table name to store rows for current run",
                                "type": "string",
                            },
                            "output_table_name": {
                                "description": "Final table where the data should be stored",
                                "type": "string",
                            },
                            "gcs_transformer_filepath": {
                                "description": "The Javascript to text transform GCS path",
                                "type": "string",
                            },
                            "gcs_schema_filepath": {
                                "description": "The JSON Schema file path",
                                "type": "string",
                            },
                            "gcs_input_directory_pattern": {
                                "description": "The GCS input file pattern",
                                "type": "string",
                            },
                            "temp_directory": {
                                "description": "The Bigquery loading temporary directory",
                                "type": "string",
                            },
                            "post_transform": {
                                "description": "Post transform data for a BigQuery operator",
                                "type": "object",
                                "properties": {
                                    "source_column": {
                                        "description": "",
                                        "type": "string",
                                    },
                                    "filter_column": {
                                        "description": "",
                                        "type": "string",
                                    },
                                    "jobs": {
                                        "type": "array",
                                        "items": {
                                            "type": "object",
                                            "properties": {
                                                "job_name_suffix": {
                                                    "description": "Job name suffix",
                                                    "type": "string",
                                                },
                                                "target_table": {
                                                    "description": "Destination dataset table",
                                                    "type": "string",
                                                },
                                                "sql_script": {
                                                    "description": "SQL script to execute",
                                                    "type": "string",
                                                },
                                                "job_params": {
                                                    "description": "Required params",
                                                    "type": "object",
                                                },
                                            },
                                            "additionalProperties": False,
                                            "required": [
                                                "job_name_suffix",
                                                "target_table",
                                                "sql_script",
                                                "job_params",
                                            ],
                                        },
                                    },
                                },
                                "additionalProperties": False,
                            },
                        },
                        "additionalProperties": False,
                        "required": [
                            "job_name_suffix",
                            "temp_table_name",
                            "output_table_name",
                            "gcs_transformer_filepath",
                            "gcs_schema_filepath",
                            "gcs_input_directory_pattern",
                            "temp_directory",
                            "post_transform",
                        ],
                    },
                },
                "required": ["name", "pubsub_subscription", "bq_load_job"],
            },
        },
        "load_to_bq_dags": {
            "description": "List of DAGS for load_to_bq_dags.py",
            "type": "array",
            "items": {
                "description": "Load to BigQuery DAG",
                "type": "object",
                "properties": {
                    "name": {
                        "description": "DAG name",
                        "type": "string",
                    },
                    "cron_schedule": {
                        "description": "Cron scheduler for this DAG",
                        "type": "string",
                    },
                    "jobs": {
                        "Description": "Jobs for DAG",
                        "type": "array",
                        "items": {
                            "type": "object",
                            "properties": {
                                "job_type": {
                                    "description": "Defines Operator to use",
                                    "type": "string",
                                    "enum": [
                                        "gcs_sensor",
                                        "bq_drop",
                                        "gcs_to_bq",
                                        "spreadsheet_to_bq",
                                        "sql_transform",
                                        "bq_to_hdx",
                                        "python_transform",
                                    ],
                                },
                                "job_id": {
                                    "description": "Job identifier",
                                    "type": "string",
                                },
                                "src_bucket": {
                                    "description": "Source bucket",
                                    "type": "string",
                                },
                                "src_path_patterns": {
                                    "description": "Source objects to match with",
                                    "type": "array",
                                    "items": {"type": "string"},
                                },
                                "mode": {
                                    "description": "GCS Prefix sensor mode",
                                    "type": "string",
                                },
                                "poke_interval": {
                                    "description": "Sensor poke interval",
                                    "type": "number",
                                    "exclusiveMinimum": 0,
                                },
                                "timeout": {
                                    "description": "Sensor timeout",
                                    "type": "number",
                                    "exclusiveMinimum": 0,
                                },
                                "dst_table": {
                                    "description": "BigQuery Table destination",
                                    "type": "string",
                                },
                                "skip_leading_rows": {
                                    "description": "Leading rows to skip",
                                    "type": "number",
                                },
                                "write_disposition": {
                                    "description": """
                                        Describes whether a mutation to a table should overwrite or append
                                    """,
                                    "type": "string",
                                    "enum": [
                                        "WRITE_DISPOSITION_UNSPECIFIED",
                                        "WRITE_EMPTY",
                                        "WRITE_TRUNCATE",
                                        "WRITE_APPEND",
                                    ],
                                },
                                "schema_filepath": {
                                    "description": "Path to schema file",
                                    "type": "string",
                                },
                                "source_format": {
                                    "description": "File format to export. Default is CSV.",
                                    "type": "string",
                                },
                                "dependencies": {
                                    "description": "Upstream dependencies",
                                    "type": "array",
                                    "items": {
                                        "description": "job name",
                                        "type": "string",
                                    },
                                    "uniqueItems": True,
                                },
                                "sql_script": {
                                    "description": "SQL file route",
                                    "type": "string",
                                },
                                "sql_params": {
                                    "description": "Params for sql file",
                                    "type": "object",
                                },
                                "dataset_name": {
                                    "description": "HDX dataset name",
                                    "type": "string",
                                },
                                "dataset_title": {
                                    "description": "HDX dataset title",
                                    "type": "string",
                                },
                                "sql": {"description": "SQL file route", "type": "string"},
                                "dataset_methodology": {
                                    "description": "HDX dataset methodology",
                                    "type": "string",
                                },
                                "dataset_locations": {
                                    "description": "List of countries datasets to add",
                                    "type": "array",
                                    "items": {
                                        "description": "ISO 3 country code or country name",
                                        "type": "string",
                                    },
                                },
                                "dataset_update_frequency": {
                                    "description": "Dataset's expected update frequency",
                                    "type": "string",
                                    "enum": [
                                        "Every day",
                                        "Every week",
                                        "Every two weeks",
                                        "Every month",
                                        "Every three months",
                                        "Every six months",
                                        "Every year",
                                        "Never",
                                    ],
                                },
                                "dataset_start_date": {
                                    "description": "Dataset start date",
                                    "type": "string",
                                },
                                "private": {
                                    "description": "Whether the dataset is private or not",
                                    "type": "boolean",
                                },
                                "resources": {
                                    "description": "HDX resources from dataset",
                                    "type": "array",
                                    "items": {
                                        "description": "HDX resource",
                                        "type": "object",
                                        "properties": {
                                            "format": {
                                                "description": "file type",
                                                "type": "string",
                                            },
                                            "description": {
                                                "description": "Description of the resource",
                                                "type": "string",
                                            },
                                        },
                                    },
                                },
                                "kwargs_mapping": {
                                    "description": "Dataframe kwargs for Python function",
                                    "type": "object",
                                },
                                "python_function": {
                                    "description": "Python function name",
                                    "type": "string",
                                },
                                "output_table": {
                                    "description": "Bigquery Output table",
                                    "type": "string",
                                },
                                "source_uris": {
                                    "description": "The source Google Cloud Storage URIs",
                                    "type": "array",
                                    "items": {
                                        "description": "Google Cloud Storage URI",
                                        "type": "string",
                                    },
                                },
                                "max_bad_records": {
                                    "description": """
                                        The maximum number of bad records that BigQuery can ignore when running the job
                                    """,
                                    "type": "number",
                                },
                                "ignore_unknown_values": {
                                    "description": """
                                        Indicates if BigQuery should allow extra values that are not represented in the table schema
                                    """,
                                    "type": "boolean",
                                },
                            },
                            "requires": ["job_type", "job_id"],
                        },
                    },
                },
            },
        },
        "ga_to_gcs": {
            "description": "Configuration for ga_to_gcs DAG",
            "type": "object",
            "properties": {
                "bucket": {
                    "description": "Bucket to store raw GA data",
                    "type": "string",
                },
            },
        },
        "great_expectations": {
            "description": "Cloud Configuration for Great Expectations",
            "type": "object",
            "properties": {
                "bucket": {
                    "description": "Bucket storing GE files for an specific environment",
                    "type": "string",
                },
                "dataset_name": {
                    "description": "Dataset to store GE temporary tables",
                    "type": "string",
                },
            },
            "additionalProperties": False,
            "required": ["bucket", "dataset_name"],
        },
    },
    "required": [
        "gcp_project_id",
        "hdx_config",
        "pubsub_to_bq_jobs",
        "load_to_bq_dags",
        "great_expectations",
    ],
    "additionalProperties": False,
}


def _validate_unique_value(items, key):
    """Validates that a key has unique values across a list of items"""
    values_set = set()
    for item in items:
        if item[key] in values_set:
            raise ValidationError(f"{key} has non-unique values: {item[key]}")
        values_set.add(item[key])


def validate_json(file_name):
    """Validates a file with jsonschema and custom validations."""
    with open(file_name, encoding="utf-8") as json_file:
        config = json.load(json_file)
        #  JsonSchema validation
        validate(instance=config, schema=_CONFIGURATION_SCHEMA)
        #  Custom validations
        to_validate = [("pubsub_to_bq_jobs", "name"), ("load_to_bq_dags", "name")]
        try:
            for field_name, map_key in to_validate:
                _validate_unique_value(config[field_name], map_key)
        except ValidationError as error:
            logging.exception(f" {file_name} validation failed: {error.message}")
            raise


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Validate config files.")
    parser.add_argument(
        "files",
        metavar="F",
        nargs="+",
        help="JSON file to process",
    )

    args = parser.parse_args()

    for filename in args.files:
        validate_json(filename)
        logging.info("%s OK!", filename)
