# Developing for airflow-jobs

- [Developing for airflow-jobs](#developing-for-airflow-jobs)
  * [Local Setup](#local-setup)
    + [Clone Gitlab Repositories](#clone-gitlab-repositories)
    + [Install Dependencies](#install-dependencies)
    + [Setup virtual environment](#setup-virtual-environment)
  * [Test code in shared dev environment](#test-code-in-shared-dev-environment)
    + [Deploying Great Expectations](#deploying-great-expectations)
  * [Deploying to Production](#deploying-to-production)
    + [Create Merge Request](#create-merge-request)
    + [Run Deployment Pipeline](#run-deployment-pipeline)
  * [Helper Tools](#helper-tools)
    + [Pydoc](#pydoc)
    + [Config Files Validation](#config-files-validation)
    + [SQL Files Validation](#sql-files-validation)
  * [Further Reading](#further-reading)
    + [Angostura Alerts Runbook](#angostura-alerts-runbook)
    + [Deploying Great Expectations](#deploying-great-expectations-1)

## Local Setup

### Clone Gitlab Repositories

First, set up an account on Gitlab. We then recommend setting up an
[SSH Key](https://docs.gitlab.com/ee/ssh/) for your Git interactions. After
that you'll need to clone this repo and also is infra dependency:

```bash
git clone git@gitlab.com:codeforvenezuela/angostura/infra.git
git clone git@gitlab.com:codeforvenezuela/angostura/airflow-jobs.git
```

### Install Dependencies

#### Google Cloud SDK: Command Line Interface

Follow the official [installation instructions](https://cloud.google.com/sdk/install)

Note: If the following error appears at some point during setup with this guide:
```
ERROR: (gcloud.composer.environments.storage.dags.delete) Error parsing [environment].
The [environment] resource is not properly specified.
Failed to find attribute [project].
```

Then run the following commands, one by one, to perform authentication and set the project attribute:
```
gcloud auth login
gcloud config set project event-pipeline
```

#### Python 3.8 and virtualenv

This project demands Python 3.8. If this is your first time installing a particular version of
Python, we recommend giving one of the following methods a try:

- [pyenv](https://github.com/pyenv/pyenv): Lets you easily switch between multiple versions of
Python. It's simple, unobtrusive, and follows the UNIX tradition of single-purpose tools that do
one thing well.
- [Conda](https://conda.io/projects/conda/en/latest/user-guide/getting-started.html): A more powerful
package and environment manager than `pyenv`, it also brings other packages by default common for
scientific and analytic workflows: NumPy, Pandas, SciPy, Matplotlib, IPython, etc.

To install the Python dependencies required by the project, you'll need to also install version
`20.5.0` or greater of [virtualenv](https://virtualenv.pypa.io/en/latest/), a tool to create
isolated Python environments. You should do this using the Python version configured with one of the
methods above.

For example if you use Conda, you could create and activate an isolated Python 3.8 environment with
the right version of virtualenv like this:

```bash
conda create -n py38 python=3.8
conda activate py38
pip3 install virtualenv==20.5.0
```

### Setup virtual environment

We have a script available to automatically setup your virtual environment. It requires having the
right version of Python and virtualenv at this point, so make sure you do this once you install
dependencies.

After that, you should just need to run:

```bash
source ./scripts/developer_env.sh
```

You will need to do this to run unit-tests and push any code as `git commit` will run
[pre-commit](https://pre-commit.com/) hooks that run linting and auto-formatting.

#### Note: Windows Operating Systems

To run this command on Windows you can use Bash.
To use Bash, right-click within the working directory and select **Git Bash here** (comes included with your GIT installation, make sure to have it installed by this point).

#### Note: Editor limitations

When using Git Hooks, editors that integrate git features may not behave correctly. With VSCODE the
pre-commit hook would [behave differently](https://github.com/microsoft/vscode/issues/90178) from
the command line git commit tool. In this case we recommend using the git command line tool for
commiting your work.

## Test code in shared dev environment

We maintain a shared CC environment for developers to test code in a place which is close to how it
would run in prod. The development environment is called **angostura-dev-cc-env** and you can find it
[here](https://console.cloud.google.com/composer/environments/detail/us-central1/angostura-dev-cc-env/monitoring?authuser=1&project=event-pipeline). You'll need to be using a Google Account with the
permissions required.

To take advantage of this environment, follow these steps:

1. Coordinate with others in [#proj-angostura](https://codeforvenezuela.slack.com/archives/CJVTN2FC7)
as this is a shared environment so you'll need to make sure no one else is testing at the same time.
2. If you need to update dependencies, navigate to the [pypi packages section](https://console.cloud.google.com/composer/environments/detail/us-central1/angostura-dev-cc-env/packages?authuser=1&project=event-pipeline)
of the environment and make your changes.
3. Run the following to automatically upload your DAG changes (for Windows, you can use Bash):
```bash
# NOTE: This script can't be used to deploy to 'prod' outside GitLab. Later in the guide you'll
# learn about how to get your changes to prod
./scripts/deploy_dags.sh dev
```
4. Click on the `"OPEN AIRFLOW UI"` button in the environment UI. Once there, manually trigger the
DAGs you want to test and verify they meet your expectations.

### Deploying Great Expectations

If you are working with Data Quality pipelines you will be working under the **great_expectations/**
folder. To upload changes in Great Expectations you can run:

```bash
# This will update the GE bucket and the GE config file in Cloud Composer.
./scripts/deploy_ge.sh dev
```

## Deploying to Production

### Create Merge Request

Create a branch for your changes and push it upstream. When you do, the command line should produce
a link for you to create a Merge Request (MR). Example output:

```bash
remote:
remote: To create a merge request for ojcm_setup_flow_imrprovements, visit:
remote:   https://gitlab.com/codeforvenezuela/angostura/airflow-jobs/-/merge_requests/new?merge_request%5Bsource_branch%5D=ojcm_setup_flow_imrprovements
remote:
```

Follow the link provided and create a MR with the template provided.

### Run Deployment Pipeline

The deployments to production are handled via CI/CD pipelines. However, the deploy stage is a
**manual job**. When your work is merged into the master branch, you will need to **activate the
deploy stage** to successfully deploy your changes to production.

To do this follow the next instructions:

1. Merge your MR to the master branch using the GitLab UI.

2. Check the Master Branch CI/CD pipeline. Access the [CI/CD pipelines](https://gitlab.com/codeforvenezuela/angostura/airflow-jobs/-/pipelines)

3. Click on the latest CI/CD pipeline having the *master* branch. Wait until every step successfully completes, and finally click on the play button for the deployment stage.

## Helper Tools

### Pydoc

Pydoc is a tool for generating navigable documentation. We encourage newcomers
to use pydoc to get to know the repository. In order to use pydoc
you first need to add the plugins folder to your python path.

```bash
export PYTHONPATH="./plugins"
python -m pydoc -b **/**
```

### Config Files Validation

The config files are the corner stone for most of the Angostura DAGs and thus
we need to be very careful when modifying these files. In order to maintain and
validate these files, we created a tool that uses jsonschema to validate them.

Read an article explaining the usage of [config files](https://www.notion.so/codeforvenezuela/airflow-jobs-Config-File-e1a2232cd08c44a5833f5516bc33594c) inside our repository.

For running a validation of your local and production config files, you can run
in your virtual environment

```bash
python ./data/config/config_validator.py \
    ./data/config/dev.json ./data/config/prod.json
```

If the config files change their structure for any reason, this tool should be
updated to validate the new schema.

### SQL Files Validation

Airflow has no tool to validate that the SQL files its DAGs use are well-defined
queries. Hence, The SQL files validator can make dry-runs of queries to validate
the queries we write.

The tool is automated in the CI/CD pipeline. In order to use the tool in local
development, you must enter the credentials of a GCP account that has the
required permissions to make bigquery jobs. We recommend
[setting the credentials environment variable](https://cloud.google.com/docs/authentication/getting-started#setting_the_environment_variable)
and the tool will automatically use that value to connect to google services.

Also, if your queries needs to set some jinja-templated variables (like many
other SQL files in the project), make sure to add an example value for them in
the file `dags/bq_sql/bq_sql_validator.py`.

To test your SQL files, simply run:

```bash
python ./plugins/bq_sql/bq_sql_validator.py <sql_files>
```

## Further Reading

### Angostura Alerts Runbook

We prepared an [article](https://www.notion.so/codeforvenezuela/Cloud-Composer-Maintenance-1c463f8dce8943a3966f99dae0d5022a)
to introduce new maintainers in the situations where an action is needed from a human. If you are a maintainer we encourage you to read the linked article to learn how to work with the messages sent to the [#angostura-alerts](https://codeforvenezuela.slack.com/archives/C015AHAEAT1) channel.

### Deploying Great Expectations

To learn more about the integration of Great Expectations with Cloud Composer, check out the [Build a Great Expectations pipeline](https://www.notion.so/codeforvenezuela/Build-a-Great-Expectations-pipeline-d59f029985dd41cbae79c9106db2dce3) guide.
