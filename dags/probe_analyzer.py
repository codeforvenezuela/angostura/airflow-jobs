"""
# DAG Summary

This DAG analyzes the _probe_ table to check for errors in the data. If an error is found, the
task will report to **Slack** the details of it. This DAG is designed to be called every 24 hours.

The **Probe** is a Kubernetes workload that tries to send timestamps to Angostura every minute.
This DAG confirms that this data is being received correctly by checking that:

- There is no missing data.
- There is no duplicated data.
- The data received has the correct distribution.

If any of these validation fails, the DAG will trigger a message to Slack
[#angostura-alerts](https://codeforvenezuela.slack.com/archives/C015AHAEAT1)
channel to report it.

If you are on local or development mode, the message will be sent to
[#angostura-dev](https://codeforvenezuela.slack.com/archives/C01UPBM9B6D)
channel instead.

Traduced from German Robayo Probe Analyzer on April 2021. (
[url](https://gitlab.com/codeforvenezuela/angostura/infra-tools/-/tree/master/charts/probe-analyzer)
)

# Mission Critical

No this is not mission critical.

# On Failure Actions

Check the slack [#angostura-alerts](https://codeforvenezuela.slack.com/archives/C015AHAEAT1)
channel for details of failure.

# Points of Contact

Write to [#proj-angostura](https://codeforvenezuela.slack.com/archives/CJVTN2FC7).

Author: Alvaro Avila - aavila@codeforvenezuela.org
"""
import os
from airflow import DAG

# pylint: disable=import-error
from operators.great_expectations_operator import GreatExpectationsOperator, CustomExpectation
from common import (
    GCP_CONN_ID,
    SLACK_CONN_ID,
    get_dag_default_args,
    get_plugins_folder,
)


dag = DAG(
    "probe_analyzer",
    default_args={
        **get_dag_default_args(),
        "start_date": "2021-05-03",
    },
    description="Probe Analyzer",
    schedule_interval="0 0 * * *",
    catchup=False,
    template_searchpath=[get_plugins_folder()],
)

dag.doc_md = __doc__


def probe_analyzer_callable(**kwargs) -> dict:
    """Set the Probe Analyzer Checkpoint runtime parameters."""
    # Step 1: Checkpoint config. This step is optional. It should only be implemented
    # if there is any parameter that needs to be calculated upon runtime.
    table_name = (
        "event-pipeline.angostura.probe"
        if os.getenv("APP_NAMESPACE") == "prod"
        else "event-pipeline.angostura_dev.probe"
    )

    execution_date = kwargs["execution_date"]
    timestamp_millis = execution_date.int_timestamp * 1000

    jinja_env = kwargs["dag"].get_template_env()
    context = {
        "params": {
            "table_name": table_name,
            "timestamp_ms": timestamp_millis,
        },
    }
    query = jinja_env.get_template("bq_sql/probe_analyzer.query.sql").render(**context)

    return {
        "validations": [
            {
                "batch_request": {
                    "datasource_name": "event-pipeline.angostura_dev",
                    "data_connector_name": "default_runtime_data_connector_name",
                    "data_asset_name": "probe_data_asset",
                    "runtime_parameters": {
                        "query": query,
                    },
                    "batch_identifiers": {
                        "pipeline_stage_name": "quality_checks",
                        "airflow_run_id": execution_date.to_date_string(),
                    },
                },
                "expectation_suite_name": "probe.quality_checks",
            }
        ]
    }


custom_expectations_modules = [
    CustomExpectation(
        module="expectations.expect_custom_column_values_to_be_unique",
        expectation="ExpectCustomColumnValuesToBeUnique",
    ),
]

my_ge_task = GreatExpectationsOperator(
    dag=dag,
    task_id="analyze_probe",
    provide_context=True,
    bigquery_conn_id=GCP_CONN_ID,
    slack_conn_id=SLACK_CONN_ID,
    checkpoint_name="probe_checkpoint",
    runtime_params_callable=probe_analyzer_callable,
    custom_expectations=custom_expectations_modules,
)
