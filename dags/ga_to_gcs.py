# pylint: disable=import-error, missing-function-docstring, missing-module-docstring

"""
#DAG Summary
This DAG is set to extract daily data from Google Analytics, process it using lists and dicts
and write it into a JSON file that is newline delimited (ND-JSON) for BigQuery.
The file is stored in a temporary directory until it's sent to GCS.

#Global constants for Google Analytics API
SCOPES:
  Set the desired level of access.
  Docs: https://developers.google.com/identity/protocols/oauth2/scopes#analyticsreporting
KEY FILE LOCATION:
  Path to the *secrets* required for authentication and access with the API.
VIEW ID:
  Target view for the reporting API.
  Docs: https://support.google.com/analytics/answer/1009618#Views&zippy=%2Cin-this-article
"""

import json
import datetime as dt
from tempfile import TemporaryDirectory

from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.contrib.hooks.gcs_hook import GoogleCloudStorageHook
from apiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials

from common import (
    get_dag_default_args,
    GCP_CONN_ID,
    load_config,
    is_prod_environment,
)

SCOPES = ["https://www.googleapis.com/auth/analytics.readonly"]
KEY_FILE_LOCATION = "/home/airflow/gcs/data/secrets/cronjob-sa.json"
VIEW_ID = "220366698"

# Setup from the config json file
config = load_config()
ga_to_gcs_config = config["ga_to_gcs"]
BUCKET = ga_to_gcs_config["bucket"]


def initialize_analytics():
    """Initializes an Analytics Reporting API V4 service object.

    Returns:
      An authorized Analytics Reporting API V4 service object.
    """
    credentials = ServiceAccountCredentials.from_json_keyfile_name(KEY_FILE_LOCATION, SCOPES)

    # Build the service object.
    analytics = build("analyticsreporting", "v4", credentials=credentials, cache_discovery=False)

    return analytics


def get_report(analytics, pagetoken, start_date: str, end_date: str):
    """Queries the Analytics Reporting API V4.

    Args:
      analytics: An authorized Analytics Reporting API V4 service object.
      pagetoken: The pagetoken value used for the pagination mechanism.
    Returns:
      The Analytics Reporting API V4 response.
    Relevant docs:
      https://developers.google.com/analytics/devguides/reporting/core/v4/rest/v4/reports/batchGet
      https://ga-dev-tools.web.app/dimensions-metrics-explorer/
    """
    return (
        analytics.reports()
        .batchGet(
            body={
                "reportRequests": [
                    {
                        # Max num of metrics is 10, max dims is 7
                        "viewId": VIEW_ID,
                        "dateRanges": [{"startDate": start_date, "endDate": end_date}],
                        "metrics": [
                            {"expression": "ga:sessions"},
                            {"expression": "ga:users"},
                            {"expression": "ga:newUsers"},
                            {"expression": "ga:pageviews"},
                        ],
                        "dimensions": [
                            {"name": "ga:date"},
                            {"name": "ga:source"},
                            {"name": "ga:country"},
                            {"name": "ga:region"},
                            {"name": "ga:city"},
                            {"name": "ga:pagePath"},
                        ],
                        "pageToken": pagetoken,  # Pagination mechanism
                        "pageSize": 10000,  # Max num of rows to get. Limit: 10K, default: 1K
                    }
                ]
            }
        )
        .execute()
    )


def process_report(response):
    """Parses and saves the Analytics Reporting API V4 response.

    Args:
      response: An Analytics Reporting API V4 response.
    Returns:
      pagetoken: An updated pagetoken value for fetching subsequent pages.
      row_counter: the number of rows in the current page.
      row_list: list of dictionaries containing all rows from the current page.
    """
    row_counter = 0  # Keep track of num of datapoints
    row_list = []  # Empty list to store row dictionaries

    for report in response.get("reports", []):
        pagetoken = report.get("nextPageToken", None)
        column_header = report.get("columnHeader", {})
        dimension_headers = column_header.get("dimensions", [])
        metric_headers = column_header.get("metricHeader", {}).get("metricHeaderEntries", [])

        for row in report.get("data", {}).get("rows", []):
            row_dict = {}
            row_counter += 1

            dimensions = row.get("dimensions", [])
            date_range_values = row.get("metrics", [])

            for header, dimension in zip(dimension_headers, dimensions):
                parsed_header, parsed_dimension = parse_values(header, dimension)
                row_dict[parsed_header] = parsed_dimension
                print(parsed_header + ": ", parsed_dimension)

            for i, values in enumerate(date_range_values):
                print("Date range:", str(i))
                for header, metric in zip(metric_headers, values.get("values")):
                    parsed_header, parsed_metric = parse_values(header.get("name"), metric)
                    row_dict[parsed_header] = parsed_metric
                    print(parsed_header + ":", parsed_metric)

            row_list.append(row_dict)

    return pagetoken, row_counter, row_list


def parse_values(header: str, value: str):
    """Parses the dimension/metric header and value. Reformats ga:date value for BigQuery.

    Args:
      value:  single dimension/metric obtained form the GA report.
      header: the dimension's/metric's header.
    Returns:
      header: parsed version of the header arg.
      value: parsed version of the value arg.
    """

    # Reformat date for BigQuery
    if header == "ga:date":
        value = dt.datetime.strptime(value, "%Y%m%d").strftime("%Y-%m-%d")

    # Clean headers from unsupported characters for BigQuery
    header = header.lstrip("ga:")

    return header, value


def json_processing(json_list, exec_date):
    """Write the list of dictionaries with the data into a JSON file

    Args:
      json_list: the list of dictionaries containing the data from GA.
      execution_date: pendulum dt object with the current airflow context exec date.
    """
    with TemporaryDirectory() as temp_dir:
        file_name = f"dag_scheduled_{exec_date.to_date_string()}.json"
        file_path = f"{temp_dir}/{file_name}"
        gcs_filepath = (
            f"ga-raw/{exec_date.year}/{exec_date.month:02d}/{exec_date.day:02d}/{file_name}"
        )

        # Create ND-JSON file
        with open(file_path, "w", encoding="utf8") as json_file:
            for record in json_list:
                json_file.write(json.dumps(record) + "\n")

        # Upload json file to GCS bucket
        gcs_hook = GoogleCloudStorageHook(google_cloud_storage_conn_id=GCP_CONN_ID)
        gcs_hook.upload(BUCKET, gcs_filepath, file_path)


def pull_ga_data(**context):
    """Get the Google Analytics data"""
    analytics = initialize_analytics()
    exec_date = context["execution_date"]

    # Set the date range for fetching the report (using pendulum)
    start_date = exec_date.to_date_string()
    end_date = start_date  # Same day

    json_list = []
    pagetoken = "0"
    datapoints = 0

    # While a nextpagetoken exists (not None), request next page of the report
    while pagetoken:
        response = get_report(analytics, pagetoken, start_date, end_date)
        pagetoken, row_counter, row_list = process_report(response)
        json_list.extend(row_list)
        datapoints += row_counter

    print("Number of Rows/Datapoints: " + str(datapoints))
    json_processing(json_list, exec_date)


def generate_dag():
    dag = DAG(
        "ga_to_gcs",
        default_args=get_dag_default_args(),
        description="Extract from Google Analytics API to GCS",
        schedule_interval="0 0 * * *",
        concurrency=3,
        catchup=False,
        is_paused_upon_creation=not is_prod_environment(),
    )

    PythonOperator(
        python_callable=pull_ga_data,
        dag=dag,
        task_id="pull_ga_data",
        retries=2,
        provide_context=True,
    )

    return dag


GA_DATA_EXTRACT_DAG = generate_dag()
