"""Dags that load data to BQ."""
import json
from datetime import timedelta
from airflow import DAG
from airflow.contrib.operators.bigquery_operator import BigQueryOperator
from airflow.contrib.operators.gcs_to_bq import GoogleCloudStorageToBigQueryOperator
from airflow.contrib.sensors.gcs_sensor import GoogleCloudStoragePrefixSensor
from airflow.contrib.operators.bigquery_table_delete_operator import BigQueryTableDeleteOperator

# pylint: disable=import-error
from operators.bigquery_google_sheet_operator import BigQueryCreateGoogleSheetTableOperator
from operators.bigquery_to_hdx_operator import BigQueryToHdxOperator
from operators.bigquery_python_operator import BigQueryPythonOperator
from common import (
    get_dag_default_args,
    load_config,
    read_local_file,
    GCP_CONN_ID,
    get_plugins_folder,
)

default_args = get_dag_default_args()
default_args["retries"] = 5
default_args["retry_delay"] = timedelta(minutes=10)


def generate_dag(dag_conf, all_config):
    """Returns a DAG definition based on the configuration given."""
    dag = DAG(
        dag_conf["name"],
        default_args=default_args,
        schedule_interval=dag_conf["cron_schedule"],
        catchup=False,
        max_active_runs=1,
        concurrency=3,
        template_searchpath=[get_plugins_folder()],
        is_paused_upon_creation=dag_conf.get("is_paused_upon_creation", True),
    )

    # Create tasks on first loop and cache them for later setting dependencies
    tasks_map = {}
    for job_conf in dag_conf["jobs"]:
        if job_conf["job_type"] == "gcs_sensor":
            task = GoogleCloudStoragePrefixSensor(
                dag=dag,
                task_id=f'gcs_sensor_{job_conf["job_id"]}',
                bucket=job_conf["src_bucket"],
                prefix=job_conf["src_path_patterns"],
                google_cloud_conn_id=GCP_CONN_ID,
                mode=job_conf["mode"],
                poke_interval=job_conf["poke_interval"],
                timeout=job_conf["timeout"],
            )
        elif job_conf["job_type"] == "bq_drop":
            task = BigQueryTableDeleteOperator(
                dag=dag,
                task_id=f'bq_drop_{job_conf["job_id"]}',
                deletion_dataset_table=job_conf["dst_table"],
                bigquery_conn_id=GCP_CONN_ID,
                ignore_if_missing=True,
            )
        elif job_conf["job_type"] == "gcs_to_bq":
            autodetect_schema = True  # allow schema auto-detection if no fixed schema is given
            schema_fields = None
            if job_conf.get("schema_filepath"):
                autodetect_schema = False
                schema_fields = json.loads(read_local_file(job_conf["schema_filepath"]))
            task = GoogleCloudStorageToBigQueryOperator(
                dag=dag,
                task_id=f'gcs_to_bq_{job_conf["job_id"]}',
                bucket=job_conf["src_bucket"],
                source_objects=job_conf["src_path_patterns"],
                destination_project_dataset_table=job_conf["dst_table"],
                write_disposition=job_conf["write_disposition"],
                skip_leading_rows=job_conf["skip_leading_rows"],
                bigquery_conn_id=GCP_CONN_ID,
                schema_fields=schema_fields,
                autodetect=autodetect_schema,
                allow_quoted_newlines=True,
                source_format=job_conf.get("source_format", "CSV"),
            )
        elif job_conf["job_type"] == "spreadsheet_to_bq":
            autodetect_schema = True  # allow schema auto-detection if no fixed schema is given
            schema_fields = None
            if job_conf.get("schema_filepath"):
                autodetect_schema = False
                schema_fields = json.loads(read_local_file(job_conf["schema_filepath"]))
            task = BigQueryCreateGoogleSheetTableOperator(
                dag=dag,
                task_id=f'create_external_table_{job_conf["job_id"]}',
                skip_leading_rows=job_conf["skip_leading_rows"],
                schema_fields=schema_fields,
                source_uris=job_conf["source_uris"],
                max_bad_records=job_conf["max_bad_records"],
                ignore_unknown_values=job_conf["ignore_unknown_values"],
                bigquery_conn_id=GCP_CONN_ID,
                autodetect=autodetect_schema,
                destination_project_dataset_table=job_conf["dst_table"],
            )
        elif job_conf["job_type"] == "sql_transform":
            task = BigQueryOperator(
                dag=dag,
                task_id=f'sql_transform_{job_conf["job_id"]}',
                use_legacy_sql=False,
                sql=read_local_file(job_conf["sql_script"]),
                destination_dataset_table=job_conf["dst_table"],
                write_disposition=job_conf["write_disposition"],
                params=job_conf["sql_params"],
                bigquery_conn_id=GCP_CONN_ID,
                create_disposition="CREATE_IF_NEEDED",
            )
        elif job_conf["job_type"] == "bq_to_hdx":
            task = BigQueryToHdxOperator(
                dag=dag,
                task_id=f'bq_to_hdx_{job_conf["job_id"]}',
                bigquery_conn_id=GCP_CONN_ID,
                # Dataset attributes
                hdx_dataset_name=job_conf["dataset_name"],
                hdx_dataset_title=job_conf["dataset_title"],
                hdx_dataset_private=job_conf["private"],
                hdx_dataset_methodology=job_conf["dataset_methodology"],
                hdx_dataset_update_frequency=job_conf["dataset_update_frequency"],
                hdx_dataset_start_date=job_conf["dataset_start_date"],
                hdx_dataset_end_date=job_conf.get("dataset_end_date"),
                hdx_dataset_ongoing=job_conf.get("dataset_ongoing", True),
                hdx_dataset_locations=job_conf["dataset_locations"],
                # Credentials management
                maintainer_id=all_config["hdx_config"]["maintainer_id"],
                organization_id=all_config["hdx_config"]["organization_id"],
                hdx_resources=job_conf["resources"],
                hdx_config=all_config["hdx_config"]["project_configuration"],
                sql=job_conf["sql"],
                params=job_conf["sql_params"],
            )
        elif job_conf["job_type"] == "python_transform":
            task = BigQueryPythonOperator(
                dag=dag,
                task_id=f'python_transform_{job_conf["job_id"]}',
                kwargs_mapping=job_conf["kwargs_mapping"],
                output_table=job_conf["output_table"],
                python_function=job_conf["python_function"],
                bigquery_conn_id=GCP_CONN_ID,
                params=job_conf["sql_params"],
            )
        tasks_map[job_conf["job_id"]] = task

    # With all tasks created, we can now chain dependencies
    for job_conf in dag_conf["jobs"]:
        for upstream_dep in job_conf.get("dependencies", []):
            # fmt: off
            tasks_map[job_conf["job_id"]] << tasks_map[upstream_dep]  # pylint: disable=pointless-statement
            # fmt: on

    return dag


# Generate DAGs dynamically by adding them to globals
conf = load_config()
for dag_config in conf["load_to_bq_dags"]:
    globals()[dag_config["name"]] = generate_dag(dag_config, conf)
