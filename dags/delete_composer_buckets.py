import os

from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.contrib.hooks.gcs_hook import GoogleCloudStorageHook

from common import (
    get_dag_default_args,
    GCP_CONN_ID,
    get_gcs_bucket,
    is_prod_environment,
)

# String that accepts an env value and can be used to identify composer bucket names
COMPOSER_BUCKET_PREFIX_FMT = "us-central1-angostura-{env}--"


def delete_old_buckets(**context):
    """Delete non-active Composer buckets under the environment (either prod or dev)"""
    active_composer_bucket_name = get_gcs_bucket()
    if not active_composer_bucket_name:
        print("No composer bucket available, you likely are running locally. Terminating early!")
        return

    gcs_hook = GoogleCloudStorageHook(google_cloud_storage_conn_id=GCP_CONN_ID)
    storage_client = gcs_hook.get_conn()

    composer_bucket_prefix = COMPOSER_BUCKET_PREFIX_FMT.format(env=os.environ["APP_NAMESPACE"])
    all_buckets = storage_client.list_buckets()
    for bucket in all_buckets:
        if bucket.name.startswith(composer_bucket_prefix):
            if bucket.name in active_composer_bucket_name:
                print(f"Skipping {bucket.name} as is the bucket currently used by the composer env")
            else:
                print(f"Forcefully deleting {bucket.name}")
                bucket.delete(force=True)


def generate_dag():
    dag = DAG(
        "delete_composer_buckets",
        default_args=get_dag_default_args(),
        description="Delete non-active Composer buckets under the environment (either prod or dev)",
        schedule_interval="0 0 * * *",
        catchup=False,
        max_active_runs=1,
        is_paused_upon_creation=not is_prod_environment(),
    )

    PythonOperator(
        python_callable=delete_old_buckets,
        dag=dag,
        task_id="delete_old_buckets",
        provide_context=True,
    )

    return dag


DELETE_OLD_BUCKETS = generate_dag()
