"""
Dags that use the DataFlowJavaOperator, DataflowDrainOperator, DataflowTemplateOperator
and BigQueryOperator.
"""
from airflow import DAG
from airflow.contrib.operators.dataflow_operator import (
    DataflowTemplateOperator,
    DataFlowJavaOperator,
)
from airflow.contrib.operators.bigquery_operator import BigQueryOperator
from airflow.contrib.operators.bigquery_to_bigquery import BigQueryToBigQueryOperator

# pylint: disable=import-error
from operators.dataflow_drain_operator import DataflowDrainOperator
from common import (
    get_dag_default_args,
    load_config,
    read_local_file,
    GCP_CONN_ID,
    is_prod_environment,
)


def generate_dag():
    """Returns a DAG to satisfy the request."""
    dag = DAG(
        "pubsub_to_bigquery",
        default_args=get_dag_default_args(),
        schedule_interval="0 0 * * *",
        catchup=False,
        max_active_runs=1,
        is_paused_upon_creation=not is_prod_environment(),
    )

    conf = load_config()
    gcs_to_bq_template = "gs://dataflow-templates/2019-07-10-00/GCS_Text_to_BigQuery"

    for job_config in conf["pubsub_to_bq_jobs"]:
        name = job_config["name"]
        pubsub_config = job_config["pubsub_subscription"]

        pubsub_task = DataFlowJavaOperator(
            dag=dag,
            task_id=f"pubsub_to_gcs_dataflow_{name}",
            job_name=f"pubsub_to_gcs_dataflow_{name}",
            gcp_conn_id=GCP_CONN_ID,
            jar="gs://angostura-dataflow-templates/jars/dataflow-jobs-bundled-0.1.jar",
            job_class="org.codeforvenezuela.jobs.PubsubToText",
            options={
                "inputSubscription": pubsub_config["subscription"],
                "outputDirectory": f'{pubsub_config["gcs_output_directory"]}',
                "outputFilenamePrefix": (
                    "dag_{{ run_id }}_" + pubsub_config["output_filename_prefix"]
                ),
                "outputFilenameSuffix": ".json",
            },
        )

        drain_task = DataflowDrainOperator(
            dag=dag,
            task_id=f"pubsub_dataflow_drain_{name}",
            job_name=f"pubsub_to_gcs_dataflow_{name}",
            gcp_conn_id=GCP_CONN_ID,
            wait_time_s=15 * 60,
        )
        # pylint: disable=pointless-statement
        drain_task << pubsub_task

        upstream_failed_drain_task = DataflowDrainOperator(
            dag=dag,
            task_id=f"pubsub_fail_dataflow_drain_{name}",
            job_name=f"pubsub_to_gcs_dataflow_{name}",
            gcp_conn_id=GCP_CONN_ID,
            wait_time_s=0,
            trigger_rule="all_failed",
        )

        upstream_failed_drain_task << pubsub_task

        # Bq load to temporary table job.
        bq_load_config = job_config["bq_load_job"]
        bq_temp_load_task = DataflowTemplateOperator(
            dag=dag,
            task_id=f'gcs_to_bq_dataflow_{bq_load_config["job_name_suffix"]}',
            template=gcs_to_bq_template,
            job_name=f'gcs_to_bq_{bq_load_config["job_name_suffix"]}',
            gcp_conn_id=GCP_CONN_ID,
            parameters={
                "javascriptTextTransformFunctionName": "transform",
                "JSONPath": bq_load_config["gcs_schema_filepath"],
                "javascriptTextTransformGcsPath": bq_load_config["gcs_transformer_filepath"],
                "inputFilePattern": bq_load_config["gcs_input_directory_pattern"],
                "outputTable": f'{conf["gcp_project_id"]}:{bq_load_config["temp_table_name"]}',
                "bigQueryLoadingTemporaryDirectory": bq_load_config["temp_directory"],
            },
        )

        bq_temp_load_task << drain_task

        # bq load job to append data to output table.
        bq_load_task = BigQueryToBigQueryOperator(
            source_project_dataset_tables=bq_load_config["temp_table_name"],
            destination_project_dataset_table=bq_load_config["output_table_name"],
            write_disposition="WRITE_APPEND",
            bigquery_conn_id=GCP_CONN_ID,
            task_id="bq_temp_to_bq",
            dag=dag,
        )

        bq_load_task << bq_temp_load_task

        # Transform tasks
        bq_tranform_config = bq_load_config.get("post_transform", [])
        for job in bq_tranform_config["jobs"]:
            sql_transform_task = BigQueryOperator(
                dag=dag,
                task_id=f'transform_sql_{job["job_name_suffix"]}',
                use_legacy_sql=False,
                sql=read_local_file(job["sql_script"]),
                params={
                    **job["job_params"],
                    "raw_events_table": f'{conf["gcp_project_id"]}.{bq_load_config["output_table_name"]}',
                },
                destination_dataset_table=job["target_table"],
                write_disposition="WRITE_TRUNCATE",
                bigquery_conn_id=GCP_CONN_ID,
            )
            sql_transform_task << bq_load_task

    return dag


PUBSUB_TO_BQ_DAG = generate_dag()
