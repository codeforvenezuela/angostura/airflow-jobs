package org.codeforvenezuela.jobs.utils;

import org.joda.time.Duration;
import org.junit.Test;

import static org.junit.Assert.*;

public class DurationUtilsTest {
    @Test
    public void testDurationIsParsedCorrectly() {
        assertEquals(DurationUtils.parseDuration("5m"), Duration.standardMinutes(5));
        assertEquals(DurationUtils.parseDuration("7h"), Duration.standardHours(7));
        assertEquals(DurationUtils.parseDuration("10s"), Duration.standardSeconds(10));
    }
}
