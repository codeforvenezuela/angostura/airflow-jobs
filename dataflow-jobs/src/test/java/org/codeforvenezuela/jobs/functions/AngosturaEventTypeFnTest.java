package org.codeforvenezuela.jobs.functions;

import org.junit.Test;

import static org.junit.Assert.*;

public class AngosturaEventTypeFnTest {
  @Test
  public void testEventIsParsedCorrectly() {
    final AngosturaEventTypeFn eventTypeFn = new AngosturaEventTypeFn();

    assertEquals(
        "probe",
        eventTypeFn.apply(
            "{\"created_at\":\"2020-03-21T19:28:58.837892054Z\",\"event\":{\"type\":\"probe\",\"version\":\"1\",\"payload\":\"dGVzdA==\"},\"ip\":\"169.254.8.129:55820\",\"user_agent\":\"curl/7.52.1\"}"));
    assertEquals("myEventType", eventTypeFn.apply("{\"event\":{\"type\":\"myEventType\"}}"));
  }
}
