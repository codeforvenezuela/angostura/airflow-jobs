package org.codeforvenezuela.jobs.functions;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.beam.sdk.transforms.SerializableFunction;

import java.io.IOException;
import java.io.UncheckedIOException;

/**
 * AngosturaEventTypeFn is an Apache Beam function that parses Angostura JSON events and gets its
 * event type.
 *
 * <p>These events look like:
 * {"created_at":"2020-03-20T03:40:09.823151398Z","event":{"type":"probe","version":"1","payload":"dGVzdA=="},"ip":"169.254.8.129:38372","user_agent":"curl/7.52.1"}
 */
public class AngosturaEventTypeFn implements SerializableFunction<String, String> {
  private transient volatile ObjectMapper objectMapper;

  public AngosturaEventTypeFn() {}

  private ObjectMapper objectMapper() {
    if (this.objectMapper == null) {
      synchronized (this) {
        if (this.objectMapper == null) {
          this.objectMapper = new ObjectMapper();
        }
      }
    }

    return objectMapper;
  }

  @Override
  public String apply(String input) {
    try {
      final AngosturaPubsubEvent pubsubEvent =
          objectMapper().readValue(input, AngosturaPubsubEvent.class);
      return pubsubEvent.event.type;
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  // Define POJO objects for Angostura events so that we can parse these using Jackson in JSON.
  static class AngosturaPubsubEvent {
    @JsonProperty("created_at")
    private String createdAt;

    @JsonProperty("ip")
    private String ip;

    @JsonProperty("user_agent")
    private String userAgent;

    @JsonProperty("event")
    private AngosturaEvent event;
  }

  static class AngosturaEvent {
    @JsonProperty("type")
    private String type;

    @JsonProperty("version")
    private String version;

    @JsonProperty("payload")
    private String payload;
  }
}
