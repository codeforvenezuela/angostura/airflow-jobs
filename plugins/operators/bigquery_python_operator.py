"""
Airflow operator used when `job_type=python_transform` in our config
"""
import importlib

from airflow.contrib.hooks.bigquery_hook import BigQueryHook
from airflow.models.baseoperator import BaseOperator
from airflow.utils.decorators import apply_defaults


class BigQueryPythonOperator(BaseOperator):  # pylint: disable=too-few-public-methods
    """
    Call a python function with a set of dataframes provided by configuration,
    and writes the result to a target BigQuery table

    :param sql_dicts: A dictionary mapping the parameters name of the function
    to the SQL query that will extract the results into a pandas DataFrame
    :type sql_dicts: dict, from str to str
    :param output_table: The BigQuery table where the resulting DataFrame will
    be written
    :type output_table: str
    :param python_function: A reference to a python function in the
    `python_transformations.py` module
    :type python_function: str
    """

    template_ext = (".sql",)
    template_fields = ("kwargs_mapping",)

    @apply_defaults
    def __init__(
        self,
        kwargs_mapping,
        output_table,
        python_function,
        bigquery_conn_id: str = "bigquery_default",
        delegate_to=None,
        *args,
        **kwargs,
    ):
        super().__init__(*args, **kwargs)
        self.bigquery_conn_id = bigquery_conn_id
        self.delegate_to = delegate_to
        self.kwargs_mapping = kwargs_mapping
        self.output_table = output_table
        self.python_function = python_function

    def execute(self, context):  # pylint: disable=unused-argument
        """Execution of the Operator."""
        bq_hook = BigQueryHook(
            bigquery_conn_id=self.bigquery_conn_id,
            use_legacy_sql=False,
            delegate_to=self.delegate_to,
        )
        dataframes = {
            arg_name: bq_hook.get_pandas_df(sql_template)
            for arg_name, sql_template in self.kwargs_mapping.items()
        }

        transformations = importlib.import_module("python_transformations")
        function = getattr(transformations, self.python_function)
        result = function(**dataframes)
        if result is not None:
            result.to_gbq(
                self.output_table,
                bq_hook._get_field("project"),  # pylint: disable=protected-access
                if_exists="replace",
            )
