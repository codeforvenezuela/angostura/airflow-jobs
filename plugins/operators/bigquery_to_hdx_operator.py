# -*- coding: utf-8 -*-
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
"""
This module contains the BigQueryToHdxOperator alongside other classes to
ease the integration between Angostura and HDX
"""

import os
import tempfile
import shutil
import logging
from typing import List
import pandas as pd

from airflow.models.baseoperator import BaseOperator
from airflow.utils.decorators import apply_defaults
from airflow.contrib.hooks.bigquery_hook import BigQueryHook
from hdx.data.dataset import Dataset
from hdx.data.hdxobject import HDXError
from hdx.data.resource import Resource
from hdx.facades.simple import facade


# pylint: disable=too-many-instance-attributes
class BigQueryToHdxOperator(BaseOperator):
    """
    Operator that takes a bigquery table, a set of columns and creates a Dataset in HDX
    with the exported columns

    :param hdx_dataset_name: The HDX's Dataset name
    :type hdx_dataset_name: str
    :param hdx_dataset_title: The HDX's Dataset title
    :type hdx_dataset_title: str
    :param hdx_dataset_private: Is the Dataset private?
    :type hdx_dataset_private: bool
    :param hdx_dataset_methodology: Data extraction methodology
    :type hdx_dataset_methodology: str. Exact values can be seen in the design-doc
    :param hdx_dataset_update_frequency: How frequent is the data updated?
    :type hdx_dataset_update_frequency: str. Exact values can be seen in the design-doc
    :param hdx_dataset_locations: Data locations
    :type hdx_dataset_locations: List[str]. Each element is an ISO 3 country code
    :param hdx_dataset_start_date: Earliest date covered by dataset
    :type hdx_dataset_start_date: str. Expected to be ISO 8601
    :param hdx_dataset_end_date: Latest date covered by dataset
    :type hdx_dataset_end_date: str. Expected to be ISO 8601
    :param hdx_dataset_ongoing: Indicates the date coverage of the dataset is ongoing
    :type hdx_dataset_ongoing: bool

    :param hdx_resources: A list of resources to be published with the dataset
    :type hdx_resources: A `List[dict]`.
    :param bq_table_name: The BigQuery full qualified table name.
    :type bq_table_name: str
    :param bq_columns: The columns of `bq_table_name` that will be exported with
    the dataset
    :type bq_columns: `List[str]`
    :param organization_id: The organization id that owns this dataset
    :type organization_id: str
    :param maintainer_id: The maintainer user id of this dataset
    :type maintainer_id: str
    :param hdx_config: hdx configuration as a dict
    :param hdx_config: dict

    """

    template_fields = ("sql",)
    template_ext = (".sql",)

    logger = logging.getLogger(__name__)
    #  pylint: disable=too-many-arguments
    @apply_defaults
    def __init__(
        self,
        hdx_dataset_name: str,
        hdx_dataset_title: str,
        hdx_dataset_private: bool,
        hdx_dataset_methodology: str,
        hdx_dataset_update_frequency: str,
        hdx_dataset_locations: List[str],
        hdx_dataset_start_date: str,
        hdx_dataset_end_date: str,
        hdx_dataset_ongoing: bool,
        hdx_resources: List[dict],
        sql: str,
        organization_id: str,
        maintainer_id: str,
        hdx_config: dict,
        bigquery_conn_id: str = "bigquery_default",
        delegate_to=None,
        test_mode=False,
        *args,
        **kwargs,
    ):
        super().__init__(*args, **kwargs)
        self.hdx_dataset_name = hdx_dataset_name
        self.hdx_dataset_title = hdx_dataset_title
        self.hdx_dataset_private = hdx_dataset_private
        self.hdx_dataset_methodology = hdx_dataset_methodology
        self.hdx_dataset_update_frequency = hdx_dataset_update_frequency
        self.hdx_dataset_locations = hdx_dataset_locations
        self.hdx_dataset_start_date = hdx_dataset_start_date
        self.hdx_dataset_end_date = hdx_dataset_end_date
        self.hdx_dataset_ongoing = hdx_dataset_ongoing
        self.hdx_resources = hdx_resources
        self.sql = sql
        self.organization_id = organization_id
        self.maintainer_id = maintainer_id
        self.hdx_config = hdx_config
        self.bigquery_conn_id = bigquery_conn_id
        self.delegate_to = delegate_to
        self.test_mode = test_mode

    def execute(self, context):
        results = self._query_bigquery()
        if not self.test_mode:
            facade(
                lambda: self._submit_to_hdx(results),
                hdx_site="prod" if os.environ.get("APP_NAMESPACE") == "prod" else "stage",
                hdx_key=os.environ.get("HDX_KEY"),
                user_agent="c4v/airflow",
                project_config_dict=self.hdx_config,
            )
        else:
            self._submit_to_hdx(results)

    def _create_hdx_dataset(self, temp_files_folder: str, results: pd.DataFrame) -> Dataset:
        """Constructs an HDX Dataset with the metadata given at object creation"""
        csv_filepath = os.path.join(temp_files_folder, self.hdx_dataset_name + ".csv")
        json_filepath = os.path.join(temp_files_folder, self.hdx_dataset_name + ".json")

        dataset = Dataset(
            {
                "name": self.hdx_dataset_name,
                "private": self.hdx_dataset_private,
                "title": self.hdx_dataset_title,
                "methodology": self.hdx_dataset_methodology,
                "license_id": "ODC-ODbL",
                "dataset_source": "Angostura",
            }
        )
        dataset.set_date_of_dataset(
            self.hdx_dataset_start_date, self.hdx_dataset_end_date, self.hdx_dataset_ongoing
        )
        dataset.set_maintainer(self.maintainer_id)
        dataset.set_organization(self.organization_id)
        dataset.add_country_locations(self.hdx_dataset_locations)
        dataset.set_expected_update_frequency(self.hdx_dataset_update_frequency)
        for resource in self.hdx_resources:
            res_format = resource["format"]
            hdx_resource = Resource(
                {
                    "name": f"{self.hdx_dataset_name}.{res_format.lower()}",
                    "description": resource["description"],
                    "format": res_format,
                }
            )
            if res_format == "CSV":
                hdx_resource.set_file_to_upload(csv_filepath)
                results.to_csv(index=False, path_or_buf=csv_filepath)
            elif res_format == "JSON":
                results.to_json(orient="records", path_or_buf=json_filepath)
                hdx_resource.set_file_to_upload(json_filepath)
            else:
                raise ValueError(str(resource.res_format) + " file type is not supported")
            dataset.add_update_resource(hdx_resource)
        return dataset

    def _submit_to_hdx(self, results: pd.DataFrame):
        temp_files_folder = tempfile.mkdtemp()
        try:
            self.logger.info("Creating HDX dataset")
            dataset = self._create_hdx_dataset(temp_files_folder, results)

            # We have some metadata fields which we only want to update through the
            # HDX web interface, so below we go through the creation flow the first
            # time only and set default values for those, but later we do updates so
            # we rely on whatever is there already
            try:
                self.logger.info("Trying to update the dataset first in case it exists")
                dataset.update_in_hdx()
            except HDXError:
                self.logger.info(
                    "Failed to update, trying creation instead with extra default metadata"
                )
                dataset["notes"] = "TBD"
                dataset.create_in_hdx()
        finally:
            shutil.rmtree(temp_files_folder)

    def _query_bigquery(self) -> pd.DataFrame:
        bq_hook = BigQueryHook(
            bigquery_conn_id=self.bigquery_conn_id,
            use_legacy_sql=False,
            delegate_to=self.delegate_to,
        )

        return bq_hook.get_pandas_df(self.sql)
