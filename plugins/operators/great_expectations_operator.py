"""
# Great Expectations Operator

This operator is designed to run Great Expectation Checkpoints. The checkpoint should be already
created and stored in the Great Expectations checkpoints store.

# Usage

For a simple usage of the operator you should only need to provide the bigquery_conn_id,
slack_conn_id and the checkpoint name. The operator will fetch the Great Expectations config file
and try to find your checkpoint in the Checkpoints Store.

If you need to define checkpoint configuration at runtime, you will need to define your own
`python_callable` that sets the required configuration. You can provide your python_callable with
the Airflow Context if you set the provide_context=True on the operator. The python_callable should
return a dict with the kwargs of a CheckpointConfig class. See the probe_analyer DAG
for an example.

# Contributors

- Alvaro Avila (aavila@codeforvenezuela.org)

"""
import logging
import uuid
from importlib import import_module
from collections import namedtuple
from typing import Callable, Optional, Union, List
from airflow.operators import BaseOperator
from airflow.utils.decorators import apply_defaults
from airflow.contrib.hooks.bigquery_hook import BigQueryHook
from airflow.hooks.base_hook import BaseHook
from airflow.contrib.operators.slack_webhook_operator import SlackWebhookHook
from airflow.exceptions import AirflowConfigException, AirflowFailException
from great_expectations.data_context import DataContext
from great_expectations.checkpoint import Checkpoint
from great_expectations.data_context.types.base import CheckpointConfig
from great_expectations.data_context.types.resource_identifiers import ValidationResultIdentifier

#  pylint: disable=import-error, wrong-import-order
from common import get_data_folder, load_config

# Class to refer to custom expectations.
CustomExpectation = namedtuple("CustomExpectation", "module expectation")

log = logging.getLogger(__name__)

config = load_config()["great_expectations"]
GREAT_EXPECTATIONS_BUCKET = config["bucket"]
GREAT_EXPECTATIONS_DATASET = config["dataset_name"]


def _get_temp_table_name() -> str:
    """Generate a random name for the GE temporary table"""
    rand_str = str(uuid.uuid4().hex)
    return f"ge_temp_{rand_str[:10]}"


def _set_validation_temp_table(validation: Union[CheckpointConfig, dict]):
    """
    Set the bigquery_temp_table for a validation batch request.

    Temporary tables are used to store the results of our custom queries. GE will use these
    tables to get batches of data to validate.
    """
    batch_request = validation["batch_request"]
    log.info(f"Checking temp_table for {batch_request['data_asset_name']}")
    try:
        bq_temp_table = batch_request["batch_spec_passthrough"]["bigquery_temp_table"]
        if GREAT_EXPECTATIONS_DATASET not in bq_temp_table:
            log.warning(f"Using dataset for temp tables different to {GREAT_EXPECTATIONS_DATASET}")
    except KeyError:
        temp_table_name = _get_temp_table_name()
        log.info(f"Generated temp_table name {temp_table_name}")
        batch_request["batch_spec_passthrough"] = {
            "bigquery_temp_table": f"{GREAT_EXPECTATIONS_DATASET}.{temp_table_name}"
        }


def _set_checkpoint_temp_tables(checkpoint: Checkpoint, checkpoint_config: dict = None):
    """
    Set the temp_table to store the GE query results for each validation in a Checkpoint.
    This function will not try to change an user-defined temp table name. It only fills
    the configuration if none is provided.

    The dataset name and temp_table name will be considered with the following priority:
        1. checkpoint_config validations configuration
        2. checkpoint_config configuration
        3. checkpoint validations configuration
        4. checkpoint configuration
    """
    log.info("Setting Temporary tables dataset...")
    try:
        if checkpoint_config["validations"]:
            validations = checkpoint_config["validations"]
            for validation in validations:
                _set_validation_temp_table(validation)
        else:
            _set_validation_temp_table(checkpoint_config)
    except TypeError:
        try:
            _checkpoint_config = checkpoint.get_substituted_config()
            if _checkpoint_config["validations"]:
                validations = _checkpoint_config["validations"]
                for validation in validations:
                    _set_validation_temp_table(validation)
            else:
                _set_validation_temp_table(_checkpoint_config)
        except TypeError:
            log.warning("No validation config found. Ommiting temp_table definition")


class GreatExpectationsOperator(BaseOperator):
    """
    An operator to leverage Great Expectations as a task in our Airflow DAG.

    :param bigquery_conn_id: Name of the BigQuery connection (as configured in Airflow) that
        contains the connection and credentials info needed to connect to BigQuery.
    :param slack_conn_id: Name of the Slack connection (as configured in Airflow) that contains
        the webhook information needed to send an alert.
    :param checkpoint_name: The Great Expectations checkpoint name to trigger with the operator.
    :param fail_task_on_validation_failure: Make the Airflow Operator Fail if the validation fails.
    :param runtime_environment: Runtime Environment variables for the DataContext configuration.
    :param runtime_params_callable: A function that the operator can use to define runtime
        parameters for the Checkpoint run.
    :param custom_expectations: A dict of custom expectations to load for the specific Checkpoint
        were each key maps to a module and each value maps to a list of attributes inside that
        module.
    """

    ui_color = "#AFEEEE"
    ui_fgcolor = "#0000000"

    @apply_defaults
    def __init__(
        self,
        bigquery_conn_id: str = "bigquery_default",
        slack_conn_id: str = "slack_default",
        checkpoint_name: str = None,
        fail_task_on_validation_failure: Optional[bool] = False,
        runtime_environment: Optional[dict] = None,
        runtime_params_callable: Optional[Callable[..., dict]] = None,
        custom_expectations: Optional[List[CustomExpectation]] = None,
        *args,
        **kwargs,
    ):
        self.bigquery_conn_id = bigquery_conn_id
        self.slack_conn_id = slack_conn_id
        if not checkpoint_name:
            raise AirflowConfigException(
                "Provide a Checkpoint name for the GreatExpectationsOperator"
            )
        self.checkpoint_name = checkpoint_name
        self.fail_task_on_validation_failure = fail_task_on_validation_failure
        if not runtime_environment:
            runtime_environment = {}
        self.runtime_environment = runtime_environment
        self.runtime_params_callable = runtime_params_callable
        self.custom_expectations = custom_expectations
        super().__init__(*args, **kwargs)

    def _get_data_context(self) -> DataContext:
        bq_conn = BigQueryHook(bigquery_conn_id=self.bigquery_conn_id)
        credentials_path = bq_conn.extras["extra__google_cloud_platform__key_path"]

        slack_conn = BaseHook.get_connection(self.slack_conn_id)
        slack_webhook = f"{slack_conn.host}/{slack_conn.password}"
        # Retrieve GE configuration
        data_context = DataContext(
            context_root_dir=get_data_folder(),
            runtime_environment={
                "GOOGLE_APPLICATION_CREDENTIALS": credentials_path,
                "SLACK_WEBHOOK": slack_webhook,
                "GREAT_EXPECTATIONS_BUCKET": GREAT_EXPECTATIONS_BUCKET,
                "GREAT_EXPECTATIONS_STORES": "GCS",
                **self.runtime_environment,
            },
        )
        return data_context

    def _on_failure_callback(self, results):
        """Send the instructions to open data docs to Slack"""
        validation_results = results["run_results"]
        url_suffix = "index.html"
        for key in validation_results.keys():
            if isinstance(key, ValidationResultIdentifier):
                identifier = "/".join(key.to_tuple())
                url_suffix = f"validations/{identifier}.html"
                break
        message = "".join(
            [
                "To see validation results run the following commands:\n",
                "```",
                f"gsutil -m copy -r gs://{GREAT_EXPECTATIONS_BUCKET}/datadocs /tmp/\n",
                f"sensible-browser /tmp/datadocs/{url_suffix}",
                "```",
            ]
        )
        slack_webhook_token = BaseHook.get_connection(self.slack_conn_id).password
        SlackWebhookHook(
            http_conn_id=self.slack_conn_id,
            webhook_token=slack_webhook_token,
            message=message,
            username="airflow",
        ).execute()

    def execute(self, context):
        log.info("Retrieving Great Expectations configuration...")
        data_context = self._get_data_context()

        if self.custom_expectations:
            for custom_expec in self.custom_expectations:
                getattr(import_module(custom_expec.module), custom_expec.expectation)

        if self.runtime_params_callable:
            checkpoint_config = self.runtime_params_callable(**context)
        else:
            checkpoint_config = None

        checkpoint = data_context.get_checkpoint(name=self.checkpoint_name)
        _set_checkpoint_temp_tables(checkpoint, checkpoint_config)

        log.info("Running validation with Great Expectations...")
        if checkpoint_config is None:
            checkpoint_config = {}
        results = checkpoint.run(**checkpoint_config)
        if not results["success"]:
            self._on_failure_callback(results)
            if self.fail_task_on_validation_failure:
                raise AirflowFailException("Validation with Great Expectations failed.")
            log.warning(
                "Validation with Great Expectations failed. Continuing DAG execution "
                "because fail_task_on_validation_failure is set to False."
            )
        else:
            log.info("Validation with Great Expectations successful.")
