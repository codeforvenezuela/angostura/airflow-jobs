"""Common functions used across DAGs."""
import json
from datetime import datetime, timedelta
import os
import textwrap
import sys
from typing import List, Callable
from airflow.hooks.base_hook import BaseHook
from airflow.contrib.operators.slack_webhook_operator import SlackWebhookOperator

# GCP_CONN_ID is the identifier to the production service account
GCP_CONN_ID = "gcp_production"

# DEFAULT_DAGS_FOLDER is the default folder in which DAGs are expected to be
DEFAULT_DAGS_FOLDER = "/home/airflow/gcs/dags"

# Identifier to the slack credentials connection
SLACK_CONN_ID = "slack_alert_bot"


def task_fail_slack_alert(context):
    """
    Common failure task that is executed when any task fails.

    This assumes that SLACK_CONN_ID connection exists. To configure it for the
    first time, go here:
    https://medium.com/datareply/integrating-slack-alerts-in-airflow-c9dcd155105
    """
    slack_webhook_token = BaseHook.get_connection(SLACK_CONN_ID).password
    slack_msg = textwrap.dedent(
        """
            :red_circle: Task Failed.
            *Task*: {task}
            *Dag*: {dag}
            *Execution Time*: {exec_date}
            *Log Url*: {log_url}
            """.format(
            task=context.get("task_instance").task_id,
            dag=context.get("task_instance").dag_id,
            exec_date=context.get("execution_date"),
            log_url=context.get("task_instance").log_url,
        )
    )
    failed_alert = SlackWebhookOperator(
        task_id="slack_test",
        http_conn_id=SLACK_CONN_ID,
        webhook_token=slack_webhook_token,
        message=slack_msg,
        username="airflow",
    )
    return failed_alert.execute(context=context)


def is_prod_environment():
    """Return True if this function is called within a prod environment"""
    return os.getenv("APP_NAMESPACE") == "prod"


def get_dag_default_args(**kargs):
    """Get general sane DAG default configuration values."""
    default_args = {
        "owner": "eng@codeforvenezuela.org",
        "depends_on_past": False,
        "start_date": datetime(2019, 12, 1),
        "email": ["eng@codeforvenezuela.com"],
        "email_on_failure": False,
        "email_on_retry": False,
        "retries": 1,
        "retry_delay": timedelta(minutes=5),
        "dataflow_default_options": {
            "project": "event-pipeline",
            "zone": "us-central1-a",
            "stagingLocation": "gs://angostura-dev/dataflow-staging-tmp-location/staging/",
            "gcpTempLocation": "gs://angostura-dev/dataflow-staging-tmp-location/tmp/",
        },
    }

    if os.getenv("APP_NAMESPACE") == "prod":
        default_args["on_failure_callback"] = task_fail_slack_alert
    default_args.update(kargs)
    return default_args


def get_dags_folder() -> str:
    """Returns the dags folder for local or prod environment."""
    return os.getenv("DAGS_FOLDER", DEFAULT_DAGS_FOLDER)


def get_gcs_bucket() -> str:
    """Returns the GCS bucket of the composer environment (or None if local)."""
    return os.getenv("GCS_BUCKET")


def get_data_folder() -> str:
    """Returns the data folder for local or prod environment."""
    return os.path.join(os.getenv("AIRFLOW_PATH", "/home/airflow/gcs"), "data")


def get_plugins_folder() -> str:
    """Returns the plugins folder for local or prod environment."""
    return os.getenv("PLUGINS_FOLDER", "/home/airflow/gcs/plugins")


def load_config():
    """Loads the json configuration for the active app namespace"""
    data_folder = get_data_folder()
    conf_filepath = os.path.join(data_folder, "config", f'{os.environ["APP_NAMESPACE"]}.json')
    with open(conf_filepath, encoding="utf-8") as j_file:
        config = json.load(j_file)
    return config


def read_local_file(filename: str) -> str:
    """Reads a file which can be either an absolute path or a relative path from the DAGs folder"""
    plugins_folder = get_plugins_folder()
    filename = filename if filename.startswith("/") else os.path.join(plugins_folder, filename)
    with open(filename, encoding="utf-8") as _file:
        return _file.read()


def bulk_sql_values_param(
    param_key: str,
    params: List[any],
    format_fn: Callable[..., str],
    **kwargs,
) -> List[dict]:
    """
    Build query_params for any query that has the `VALUES` keyword, such as `INSERT` queries.
    This method populates the field with several values, hence making the query a bulk insert
    statement. This is used to solve the large-query problem.

    Running `INSERT` queries with this helper function has a better performance than the
    `BigQueryCursor.executemany()` method, which actually makes a query for each element in params.

    The max length of each param is defined based on the resources limitations of the GCP
    project. Each query_param will not exceed the limit of 2 ** 16 bytes.

    The query must be in the form `INSERT [INTO] target_name VALUES {{ param_key }}`. Note that
    the query uses jinja templating for the parameter.

    You can also use modulus substitution with `INSERT [INTO] target_name VALUES %(param_key)s`.

    Each expr will be defined by the `format_fn`, which should receive an item from the params
    list and should return a string containing the desired expression.

    Example
    -----------
    >>> params = [{"key": "value_1"}, {"key": "value_2"}]
    >>> bulk_sql_values_param("values", params, lambda x: "'" + x["key"] + "'",
    ...     other_param="other_value")
    [{"values": "('value_1'),('value_2')", "other_param": "other_value"}]
    """
    # Since bigquery accepts queries of at most 1024KB, we need to split the query in multiple
    # queries if the params list is too large.
    query_params = []
    query_str = ""
    for param in params:
        if len(query_str) == 0:
            query_str = format_fn(param)
        else:
            query_str = "),(".join([query_str, format_fn(param)])
        # 1024KB limit would leave us 2 ** 19 bytes as an upper bound for the query_str, but
        # because of resources limitations, we can only use up to 2 ** 17 bytes in each query.
        if sys.getsizeof(query_str) > 2 ** 16:
            # append accumulated query_params and reset query_str
            query_params.append({param_key: f"({query_str})", **kwargs})
            query_str = ""
    if len(query_str) > 0:
        query_params.append({param_key: f"({query_str})", **kwargs})
    return query_params
