"""Class that expose all operators to Composer"""

from airflow.plugins_manager import AirflowPlugin

# pylint: disable=import-error
from operators.bigquery_google_sheet_operator import BigQueryCreateGoogleSheetTableOperator
from operators.bigquery_to_hdx_operator import BigQueryToHdxOperator
from operators.bigquery_python_operator import BigQueryPythonOperator
from operators.dataflow_drain_operator import DataflowDrainOperator
from operators.great_expectations_operator import GreatExpectationsOperator


class GoogleComputeEnginePlugin(AirflowPlugin):
    """Expose Airflow Operators"""

    name = "cc_plugins"
    operators = [
        BigQueryCreateGoogleSheetTableOperator,
        BigQueryToHdxOperator,
        BigQueryPythonOperator,
        DataflowDrainOperator,
        GreatExpectationsOperator,
    ]
