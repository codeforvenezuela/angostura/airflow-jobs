SELECT timestamp
FROM `{{ params.table_name }}`
WHERE unix_millis(timestamp_sub(TIMESTAMP_MILLIS({{ params.timestamp_ms }}), INTERVAL 36 hour)) <= timestamp
  AND timestamp <= unix_millis(timestamp_sub(TIMESTAMP_MILLIS({{ params.timestamp_ms }}), INTERVAL 12 hour))
ORDER BY timestamp;
