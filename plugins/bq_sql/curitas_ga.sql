/*
The schema (with the raw input data) is the following:

    date        STRING  REQUIRED
    source      STRING  REQUIRED
    country     STRING  REQUIRED
    city        STRING  REQUIRED
    region      STRING  REQUIRED
    pagePath    STRING  REQUIRED
    sessions    STRING  REQUIRED
    users       STRING  REQUIRED
    newUsers    STRING  REQUIRED
    pageViews   STRING  REQUIRED

    -The current script casts the date string to a 'date' type.
    -Sessions, users, newusers, pageViews are cast as 'int64'.
    -pagePath is used to generate a new column containing the medicine names from the URLs.

    POINT OF CONTACT
    NICOLÁS SILVEIRA
    nicolas.silveira@codeforvenezuela.org
*/

SELECT DISTINCT
    SAFE_CAST (date AS date) AS date,
    source,
    country,
    region,
    city,
    pagePath,

    --New column 'medicine' based on pagePath
    CASE
        WHEN pagePath LIKE "/medicamentos/%"
            THEN REGEXP_EXTRACT(pagePath, r"^/medicamentos/([^?]+)")
    END AS medicine,


    SAFE_CAST (sessions AS int64) AS sessions,
    SAFE_CAST (users AS int64) AS users,
    SAFE_CAST (newUsers AS int64) AS newUsers,
    SAFE_CAST (pageViews AS int64) AS pageViews
FROM `{{ params.src_table }}`
