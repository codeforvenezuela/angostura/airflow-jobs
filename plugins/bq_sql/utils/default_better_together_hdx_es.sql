-- All BetterTogether tables share some columns which we only want to share
-- to consumers on a request basis. The remaining are safe to share publicly
SELECT * EXCEPT(
    -- We are only ok with a daily granularity, so avoid exposing HH:MM:SS
    tiempo_de_envio,

    -- UserID is obfuscated, but it can be used to join across datasets so
    -- there's risk of giving out more information than we anticipate
    id_de_usuario,

    -- Avoid exposing anything more than the submission state
    municipio_de_envio,
    parroquia_de_envio
) FROM `{{ params.table_name }}`
