SELECT * EXCEPT(
    -- We are only ok with a daily granularity, so avoid exposing HH:MM:SS
    submission_timestamp,

    -- UserID is obfuscated, but it can be used to join across datasets so
    -- there's risk of giving out more information than we anticipate
    user_id,

    -- Avoid exposing anything more than the submission state
    submission_municipality,
    submission_parish,

    -- Avoid columns which have been found to exhibit data quality problems
    how_many_people_ages_0_to_4_live_in_the_household,
    how_many_girls_ages_0_to_4_live_in_the_household,
    how_many_people_ages_5_to_13_live_in_the_household,
    how_many_girls_ages_5_to_13_live_in_the_household,
    how_many_people_ages_14_to_17_live_in_the_household,
    how_many_girls_ages_14_to_17_live_in_the_household,
    how_many_people_ages_18_to_49_live_in_the_household,
    how_many_females_ages_18to_49_live_in_the_household,
    how_many_people_ages_50_to_64_live_in_the_household,
    how_many_females_ages_50_to_64_live_in_the_household,
    how_many_people_ages_65_live_in_the_household,
    how_many_females_ages_65_live_in_the_household
)
FROM `{{ params.table_name }}`
