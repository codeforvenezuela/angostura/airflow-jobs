-- All BetterTogether tables share some columns which we only want to share
-- to consumers on a request basis. The remaining are safe to share publicly
SELECT * EXCEPT(
    -- We are only ok with a daily granularity, so avoid exposing HH:MM:SS
    submission_timestamp,

    -- UserID is obfuscated, but it can be used to join across datasets so
    -- there's risk of giving out more information than we anticipate
    user_id,

    -- Avoid exposing anything more than the submission state
    submission_municipality,
    submission_parish
) FROM `{{ params.table_name }}`
