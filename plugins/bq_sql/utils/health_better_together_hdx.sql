-- All BetterTogether tables share some columns which we only want to share
-- to consumers on a request basis. On top of that, the Health Access survey
-- task has some open text answers we don't want to share
SELECT * EXCEPT(
    -- We are only ok with a daily granularity, so avoid exposing HH:MM:SS
    submission_timestamp,

    -- UserID is obfuscated, but it can be used to join across datasets so
    -- there's risk of giving out more information than we anticipate
    user_id,

    -- Avoid exposing anything more than the submission state
    submission_municipality,
    submission_parish,

    --
    other_type_of_visited_center,
    other_reason_of_visit,
    other_reason_for_no_attention_during_visit,
    other_who_attended_visit,
    other_medication_source,
    other_mode_of_transport_for_medication,
    other_type_of_chronic_health_conditions_requiring_medicine,
    other_reason_for_3months_medications_supply,
    other_currency_for_buying_medications
) FROM `{{ params.table_name }}`
