/*

The schema is the following:

    id	INTEGER	NULLABLE
    created_at	TIMESTAMP	NULLABLE
    full_text	STRING	NULLABLE
    metadata	STRING	NULLABLE
    user	STRING	NULLABLE
    geo	STRING	NULLABLE
    place	STRING	NULLABLE

See tweet_example_payload.json for a sample where this SQL will run.
*/

SELECT
  SAFE_CAST(JSON_EXTRACT_SCALAR(payload, "$.tweet_payload.id") as int64) as id,
  JSON_EXTRACT_SCALAR(payload, "$.tweet_payload.created_at") as created_at,
  JSON_EXTRACT_SCALAR(payload, "$.tweet_payload.full_text") as full_text,
  JSON_EXTRACT(payload, "$.tweet_payload.metadata") as metadata,
  JSON_EXTRACT(payload, "$.tweet_payload.user") as user,
  JSON_EXTRACT(payload, "$.tweet_payload.geo") as geo,
  JSON_EXTRACT(payload, "$.tweet_payload.place") as place,
FROM `{{ params.raw_events_table }}`
WHERE event = 'tweet'
  AND JSON_EXTRACT_SCALAR(payload, '$.hashtag') = '{{ params.hashtag }}';
