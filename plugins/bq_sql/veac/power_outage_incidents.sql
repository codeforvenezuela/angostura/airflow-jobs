WITH incidents AS (
  SELECT 
   JSON_EXTRACT_SCALAR(payload, "$['id']") AS id,   
    DATE(DATE_TRUNC(created_at, DAY)) as date,
    JSON_EXTRACT_SCALAR(payload, "$['LocationID' ]") AS device_id,
     PARSE_TIMESTAMP('%Y-%m-%dT%H:%M:%E*S%Ez',JSON_EXTRACT_SCALAR(payload, "$['StartTime' ]")) AS start_time,
        PARSE_TIMESTAMP('%Y-%m-%dT%H:%M:%E*S%Ez',JSON_EXTRACT_SCALAR(payload, "$['EndTime' ]")) AS end_time,
  FROM `event-pipeline.angostura.raw_events`
  WHERE event = 'power_outage_incident'
  AND JSON_EXTRACT_SCALAR(payload, "$['LocationID' ]") IS NOT NULL
  UNION ALL 
 SELECT 
    JSON_EXTRACT_SCALAR(payload, "$['id']") AS id,
    DATE(DATE_TRUNC(created_at, DAY)) as date,
    JSON_EXTRACT_SCALAR(payload, "$['device_id' ]") AS device_id,
     PARSE_TIMESTAMP('%Y-%m-%dT%H:%M:%E*S%Ez',JSON_EXTRACT_SCALAR(payload, "$['start_time' ]")) AS start_time,
        PARSE_TIMESTAMP('%Y-%m-%dT%H:%M:%E*S%Ez',JSON_EXTRACT_SCALAR(payload, "$['end_time' ]")) AS end_time,
  FROM `event-pipeline.angostura.raw_events`
  WHERE event = 'power_outage_incident'
  AND JSON_EXTRACT_SCALAR(payload, "$['device_id' ]") IS NOT NULL
)
select 
   incidents.id,
   device_id, 
   m.city,
   m.lat,
   m.long,
   m.municipality,
   m.parish,
DATE(DATE_TRUNC(DATETIME(start_time, 'America/Caracas'), DAY)) as start_date,  
DATE(DATE_TRUNC(DATETIME(end_time, 'America/Caracas'), DAY)) as end_date,  
FORMAT_TIMESTAMP('%r', DATETIME(start_time, 'America/Caracas')) as start_hour, 
FORMAT_TIMESTAMP('%r', DATETIME(end_time, 'America/Caracas')) as end_hour,
   TIMESTAMP_DIFF(end_time, start_time,  MINUTE) as duration,
start_time,
end_time,
from incidents
JOIN angostura.veac_monitors as m on incidents.device_id =m.id
where TIMESTAMP_DIFF(end_time, start_time,  MINUTE) > 0
order by start_date desc