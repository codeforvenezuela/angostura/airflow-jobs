WITH safe_ira AS ( -- grab subset of columns from raw responses needed and safely cast to date/timestamps
    SELECT
        SAFE.PARSE_TIMESTAMP("%m/%d/%Y %H:%M:%S", timestamp) AS `timestamp`,
        SAFE.PARSE_DATE("%d-%m-%Y",
        REGEXP_EXTRACT(report_day, r"\(([0-9-]+)\)")) AS report_day ,
        hospital_code ,
        icu_beds_count ,
        icu_beds_added_since_last_report ,
        er_beds_count ,
        er_beds_added_since_last_report ,
        respiratory_units_count ,
        respiratory_units_added_since_last_report ,
        adhoc_ira_capacity_added ,
        adhoc_ira_capacity_added_details ,
        adhoc_ira_capacity_respiratory_units_available ,
        masks_available ,
        gloves_available ,
        soap_available ,
        hand_sanitizer_available ,
        ira_cases_in_uci_count ,
        other_cases_in_uci_count ,
        ira_cases_in_er_count ,
        other_cases_in_er_count ,
        ira_cases_in_respiratory_count ,
        ira_death_count ,
        covid_cases_suspected ,
        covid_cases_suspected_hospitalized_count ,
        covid_cases_confirmed_rapid_test ,
        covid_cases_confirmed_rapid_test_hospitalized_count ,
        covid_cases_confirmed_test ,
        covid_cases_confirmed_test_hospitalized_count ,
        humanitarian_aid_available ,
        humanitarian_aid_available_details ,
        power_availability ,
        wash_availability
    FROM `{{ params.src_table }}`
), clean_ira AS ( -- move forward only with non-duplicate rows that have both timestamp and report_day defined
    SELECT DISTINCT *
    FROM safe_ira
    WHERE `timestamp` IS NOT NULL AND report_day IS NOT NULL
),recent_ts AS ( -- determine the timestamp of the last report per hospital and day
    SELECT
        report_day,
        hospital_code,
        max(timestamp) AS last_report_ts
    FROM clean_ira
    GROUP BY 1, 2
), recent_ira AS ( -- use the timestamps of last report to grab just those rows from all
    SELECT
        ira.*
    FROM clean_ira AS ira
        JOIN recent_ts AS ts
    ON ira.report_day = ts.report_day
        AND ira.hospital_code = ts.hospital_code
        AND ira.timestamp = ts.last_report_ts
), unique_days AS ( -- get all distinct report days
    SELECT DISTINCT
        report_day
    FROM recent_ira
), unique_codes AS ( -- get all distinct hospital codes
    SELECT DISTINCT
        hospital_code
    FROM recent_ira
), full_unique AS ( -- get all distinct pairs of report days and hospital codes
    SELECT
    *
    FROM unique_days, unique_codes
), full_agg AS ( -- left join into all day/code combinations to use past data to fill gaps in unreported days
    SELECT
        fu.hospital_code ,
        fu.report_day ,
        EXTRACT(DATETIME FROM ira.timestamp AT TIME ZONE "America/Caracas") AS last_report_ts ,
        COALESCE(ira.ira_death_count, 0) AS ira_death_count ,
        MAX(fu.report_day)
            OVER ( ORDER BY fu.report_day DESC ROWS UNBOUNDED PRECEDING ) AS max_report_day ,
        COALESCE(LAST_VALUE(icu_beds_count IGNORE NULLS)
            OVER ( PARTITION BY fu.hospital_code ORDER BY fu.report_day ASC ROWS UNBOUNDED PRECEDING ), 0)
            AS icu_beds_count ,
        COALESCE(LAST_VALUE (icu_beds_added_since_last_report IGNORE NULLS)
            OVER ( PARTITION BY fu.hospital_code ORDER BY fu.report_day ASC ROWS UNBOUNDED PRECEDING ), "Sin reportar")
            AS icu_beds_added_since_last_report ,
        COALESCE(LAST_VALUE (er_beds_count IGNORE NULLS)
            OVER ( PARTITION BY fu.hospital_code ORDER BY fu.report_day ASC ROWS UNBOUNDED PRECEDING ), 0)
            AS er_beds_count ,
        COALESCE(LAST_VALUE (er_beds_added_since_last_report IGNORE NULLS)
            OVER ( PARTITION BY fu.hospital_code ORDER BY fu.report_day ASC ROWS UNBOUNDED PRECEDING ), "Sin reportar")
            AS er_beds_added_since_last_report ,
        COALESCE(LAST_VALUE (respiratory_units_count IGNORE NULLS)
            OVER ( PARTITION BY fu.hospital_code ORDER BY fu.report_day ASC ROWS UNBOUNDED PRECEDING ), 0)
            AS respiratory_units_count ,
        COALESCE(LAST_VALUE (adhoc_ira_capacity_added IGNORE NULLS)
            OVER ( PARTITION BY fu.hospital_code ORDER BY fu.report_day ASC ROWS UNBOUNDED PRECEDING ), "Sin reportar")
            AS adhoc_ira_capacity_added ,
        COALESCE(LAST_VALUE (adhoc_ira_capacity_added_details IGNORE NULLS)
            OVER ( PARTITION BY fu.hospital_code ORDER BY fu.report_day ASC ROWS UNBOUNDED PRECEDING ), "Sin reportar")
            AS adhoc_ira_capacity_added_details ,
        COALESCE(LAST_VALUE (adhoc_ira_capacity_respiratory_units_available IGNORE NULLS)
            OVER ( PARTITION BY fu.hospital_code ORDER BY fu.report_day ASC ROWS UNBOUNDED PRECEDING ), "Sin reportar")
            AS adhoc_ira_capacity_respiratory_units_available ,
        COALESCE(LAST_VALUE (masks_available IGNORE NULLS)
            OVER ( PARTITION BY fu.hospital_code ORDER BY fu.report_day ASC ROWS UNBOUNDED PRECEDING ), "Sin reportar")
            AS masks_available ,
        COALESCE(LAST_VALUE (gloves_available IGNORE NULLS)
            OVER ( PARTITION BY fu.hospital_code ORDER BY fu.report_day ASC ROWS UNBOUNDED PRECEDING ), "Sin reportar")
            AS gloves_available ,
        COALESCE(LAST_VALUE (soap_available IGNORE NULLS)
            OVER ( PARTITION BY fu.hospital_code ORDER BY fu.report_day ASC ROWS UNBOUNDED PRECEDING ), "Sin reportar")
            AS soap_available ,
        COALESCE(LAST_VALUE (hand_sanitizer_available IGNORE NULLS)
            OVER ( PARTITION BY fu.hospital_code ORDER BY fu.report_day ASC ROWS UNBOUNDED PRECEDING ), "Sin reportar")
            AS hand_sanitizer_available ,
        COALESCE(LAST_VALUE (ira_cases_in_uci_count IGNORE NULLS)
            OVER ( PARTITION BY fu.hospital_code ORDER BY fu.report_day ASC ROWS UNBOUNDED PRECEDING ), 0)
            AS ira_cases_in_uci_count ,
        COALESCE(LAST_VALUE (other_cases_in_uci_count IGNORE NULLS)
            OVER ( PARTITION BY fu.hospital_code ORDER BY fu.report_day ASC ROWS UNBOUNDED PRECEDING ), 0)
            AS other_cases_in_uci_count ,
        COALESCE(LAST_VALUE (ira_cases_in_er_count IGNORE NULLS)
            OVER ( PARTITION BY fu.hospital_code ORDER BY fu.report_day ASC ROWS UNBOUNDED PRECEDING ), 0)
            AS ira_cases_in_er_count ,
        COALESCE(LAST_VALUE (other_cases_in_er_count IGNORE NULLS)
            OVER ( PARTITION BY fu.hospital_code ORDER BY fu.report_day ASC ROWS UNBOUNDED PRECEDING ), 0)
            AS other_cases_in_er_count ,
        COALESCE(LAST_VALUE (ira_cases_in_respiratory_count IGNORE NULLS)
            OVER ( PARTITION BY fu.hospital_code ORDER BY fu.report_day ASC ROWS UNBOUNDED PRECEDING ), 0)
            AS ira_cases_in_respiratory_count ,
        COALESCE(LAST_VALUE (covid_cases_suspected IGNORE NULLS)
            OVER ( PARTITION BY fu.hospital_code ORDER BY fu.report_day ASC ROWS UNBOUNDED PRECEDING ), "Sin reportar")
            AS covid_cases_suspected ,
        COALESCE(LAST_VALUE (covid_cases_suspected_hospitalized_count IGNORE NULLS)
            OVER ( PARTITION BY fu.hospital_code ORDER BY fu.report_day ASC ROWS UNBOUNDED PRECEDING ), 0)
            AS covid_cases_suspected_hospitalized_count ,
        COALESCE(LAST_VALUE (covid_cases_confirmed_rapid_test IGNORE NULLS)
            OVER ( PARTITION BY fu.hospital_code ORDER BY fu.report_day ASC ROWS UNBOUNDED PRECEDING ), "Sin reportar")
            AS covid_cases_confirmed_rapid_test ,
        COALESCE(LAST_VALUE (covid_cases_confirmed_rapid_test_hospitalized_count IGNORE NULLS)
            OVER ( PARTITION BY fu.hospital_code ORDER BY fu.report_day ASC ROWS UNBOUNDED PRECEDING ), 0)
            AS covid_cases_confirmed_rapid_test_hospitalized_count ,
        COALESCE(LAST_VALUE (covid_cases_confirmed_test IGNORE NULLS)
            OVER ( PARTITION BY fu.hospital_code ORDER BY fu.report_day ASC ROWS UNBOUNDED PRECEDING ), "Sin reportar")
            AS covid_cases_confirmed_test ,
        COALESCE(LAST_VALUE (covid_cases_confirmed_test_hospitalized_count IGNORE NULLS)
            OVER ( PARTITION BY fu.hospital_code ORDER BY fu.report_day ASC ROWS UNBOUNDED PRECEDING ), 0)
            AS covid_cases_confirmed_test_hospitalized_count ,
        COALESCE(LAST_VALUE (humanitarian_aid_available IGNORE NULLS)
            OVER ( PARTITION BY fu.hospital_code ORDER BY fu.report_day ASC ROWS UNBOUNDED PRECEDING ), "Sin reportar")
            AS humanitarian_aid_available ,
        COALESCE(LAST_VALUE (humanitarian_aid_available_details IGNORE NULLS)
            OVER ( PARTITION BY fu.hospital_code ORDER BY fu.report_day ASC ROWS UNBOUNDED PRECEDING ), "Sin reportar")
            AS humanitarian_aid_available_details ,
        COALESCE(LAST_VALUE (power_availability IGNORE NULLS)
            OVER ( PARTITION BY fu.hospital_code ORDER BY fu.report_day ASC ROWS UNBOUNDED PRECEDING ), "Sin reportar")
            AS power_availability ,
        COALESCE(LAST_VALUE (wash_availability IGNORE NULLS)
            OVER ( PARTITION BY fu.hospital_code ORDER BY fu.report_day ASC ROWS UNBOUNDED PRECEDING ), "Sin reportar")
            AS wash_availability
    FROM full_unique AS fu
        LEFT JOIN recent_ira AS ira
    ON fu.report_day = ira.report_day
        AND fu.hospital_code = ira.hospital_code
), recent_report AS ( -- determine the last day reported per hospital
    SELECT
        hospital_code,
        max(report_day) AS last_reported_day
    FROM recent_ira
    GROUP BY 1
), final_agg AS ( -- combine all of the above with the hospitals table for a more rich version of IRA survey
    SELECT
        CURRENT_DATETIME("America/Caracas") AS update_time ,
        h.hospital AS hospital_name ,
        CASE h.federal_entity
            WHEN "Dtto Capital"
            THEN "Distrito Capital"
            ELSE CONCAT(h.federal_entity)
            END as
                federal_entity ,
                rr.last_reported_day ,
                rr.last_reported_day = agg.report_day AS is_latest_report ,
                rr.last_reported_day = agg.max_report_day AS is_today_report ,
                agg.*
    FROM full_agg AS agg
        LEFT JOIN `event-pipeline.angostura.hospitals` AS h
    ON agg.hospital_code = h.hospital_code
        LEFT JOIN recent_report AS rr
    ON agg.hospital_code = rr.hospital_code
)
SELECT * FROM final_agg
