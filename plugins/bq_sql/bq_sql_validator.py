"""
This file parses BigQuery queries and makes dry-runs in order to validate their syntax.

When defining a new sql file, the CI/CD pipeline will lookup for that sql and pass it to this file.

In order to run queries that use Jinja templated variables, you must update the _SQL_VARIABLES
variable down below :).

# How to use:

```bash
bq_sql_validator.py <files_list>
```

# Contributors:

- Alvaro Avila (aavila@codeforvenezuela.org)

"""
import logging
import argparse
import jinja2
from google.cloud import bigquery

logging.basicConfig(level=logging.DEBUG)
# We define how queries will run in the following variable.
_JOB_CONFIG = bigquery.QueryJobConfig(dry_run=True, use_query_cache=False)
# For queries that implement jinja templating, we define our jinja env as defined in
# airflow/models/baseoperator.py
_JINJA_ENV = jinja2.Environment(cache_size=0)

# For queries that require variables, define example values for them.
_SQL_VARIABLES = {
    "select_star.sql": {
        "params": {
            "table_name": "event-pipeline.angostura_dev.probe",
        },
    },
    "default_better_together_hdx_es.sql": {
        "params": {
            "table_name": "event-pipeline.better_together.daily_food_consumption_es_dev",
        },
    },
    "default_better_together_hdx.sql": {
        "params": {
            "table_name": "event-pipeline.better_together.daily_food_consumption_dev",
        },
    },
    "health_better_together_hdx.sql": {
        "params": {
            "table_name": "event-pipeline.better_together.monthly_health_care_access_dev",
        },
    },
    "better_together_es_long.sql": {
        "params": {
            "table_name": "event-pipeline.better_together.weekly_food_consumption_long_dev",
        },
    },
    "one_time_covid_education_impact.sql": {
        "params": {
            "src_table": "event-pipeline.better_together_raw.one_time_covid_education_impact_dev",
        },
    },
    "monthly_livelihood.sql": {
        "params": {
            "src_table": "event-pipeline.better_together_raw.monthly_livelihood_dev",
        },
    },
    "weekly_humanitarian_indicators.sql": {
        "params": {
            "src_table": "event-pipeline.better_together.weekly_food_consumption_dev",
        },
    },
    "daily_humanitarian_indicators.sql": {
        "params": {
            "src_table": "event-pipeline.better_together.daily_food_consumption_dev",
        },
    },
    "monthly_health_care_access_long.sql": {
        "params": {
            "src_table": "event-pipeline.better_together_raw.monthly_health_care_access_dev",
        },
    },
    "weekly_basic_services_delivery.sql": {
        "params": {
            "src_table": "event-pipeline.better_together_raw.weekly_basic_services_delivery_dev",
        },
    },
    "monthly_health_care_access.sql": {
        "params": {
            "src_table": "event-pipeline.better_together_raw.monthly_health_care_access_dev",
        },
    },
    "one_time_covid_impact.sql": {
        "params": {
            "src_table": "event-pipeline.better_together_raw.one_time_covid_impact_dev",
        },
    },
    "monthly_transportation_access_mobility.sql": {
        "params": {
            "src_table": "event-pipeline.better_together_raw.monthly_transportation_access_mobility_dev",
        },
    },
    "all_sectors_rollup.sql": {
        "params": {
            "daily_food_table": "event-pipeline.better_together.daily_food_consumption_dev",
            "weekly_food_table": "event-pipeline.better_together.weekly_food_consumption_dev",
            "monthly_health_table": "event-pipeline.better_together.monthly_health_care_access_dev",
        },
    },
    "daily_food_consumption.sql": {
        "params": {
            "src_table": "event-pipeline.better_together_raw.daily_food_consumption_dev",
        },
    },
    "one_time_internal_displacement.sql": {
        "params": {
            "src_table": "event-pipeline.better_together_raw.one_time_internal_displacement_dev",
        },
    },
    "weekly_food_consumption.sql": {
        "params": {
            "src_table": "event-pipeline.better_together_raw.weekly_food_consumption_dev",
        },
    },
    "daily_humanitarian_indicators_es.sql": {
        "params": {
            "src_table": "event-pipeline.better_together.daily_humanitarian_indicators_dev",
        },
    },
    "weekly_food_consumption_long.sql": {
        "params": {
            "src_table": "event-pipeline.better_together_raw.weekly_food_consumption_dev",
        },
    },
    "weekly_water_access.sql": {
        "params": {
            "src_table_v1": "event-pipeline.better_together_raw.weekly_water_access_v1_dev",
            "src_table_v2": "event-pipeline.better_together_raw.weekly_water_access_v2_dev",
        },
    },
    "daily_food_consumption_es.sql": {
        "params": {
            "src_table": "event-pipeline.better_together.daily_food_consumption_dev",
        },
    },
    "covid_hospital_ira.sql": {
        "params": {
            "raw_events_table": "event-pipeline.angostura_dev.raw_events",
        },
    },
    "probe_analyzer.query.sql": {
        "params": {
            "table_name": "event-pipeline.angostura_dev.probe",
            "timestamp_ms": "1623196800000",  # 2021-06-09T00:00:00+00:00
        },
    },
    "probe.sql": {
        "params": {
            "raw_events_table": "event-pipeline.angostura_dev.raw_events",
        },
    },
    "meditweet.sql": {
        "params": {
            "raw_events_table": "event-pipeline.angostura_dev.raw_events",
            "hashtag": "servicio_publico",
        },
    },
    "covid_bot_diagnostic.sql": {
        "params": {
            "raw_events_table": "event-pipeline.angostura_dev.raw_events",
        },
    },
    "rich_covid_hospital_ira_response.sql": {
        "params": {
            "src_table": "event-pipeline.angostura_dev.covid_hospital_ira_response_raw",
        },
    },
    "eh_survey.sql": {
        "params": {
            "raw_events_table": "event-pipeline.angostura_dev.raw_events",
        },
    },
    "get_scraped_urls.sql": {
        "table_name": "event-pipeline.sambil_collab_dev.scraped_raw",
        "website": "primicia.com.ve",
    },
    "insert.sql": {
        "table_name": "event-pipeline.angostura_dev.probe",
        "column_names": "timestamp",
        "values": "(1623196800000)",  # 2021-06-09T00:00:00+00:00
    },
    "one_time_household_demographics.sql": {
        "params": {
            "src_table": "better_together_raw.one_time_household_demographics_dev",
        },
    },
    "household_demographics_hdx.sql": {
        "params": {
            "table_name": "better_together.one_time_household_demographics_dev",
        },
    },
    "one_time_household_education.sql": {
        "params": {
            "src_table": "better_together_raw.one_time_household_education_dev",
        },
    },
    "curitas_ga.sql": {
        "params": {
            "src_table": "event-pipeline.angostura_dev.curitas_ga_raw",
        },
    },
}


def validate_sql(file_name: str, bq_client: bigquery.Client):
    """Parses a sql file and runs a dry-run in bigquery."""
    filename_key = next((key for key in _SQL_VARIABLES if file_name.endswith(key)), None)
    with open(file_name, encoding="UTF-8") as sql_file:
        parsed_sql = (
            _JINJA_ENV.from_string(sql_file.read()).render(**_SQL_VARIABLES[filename_key])
            if filename_key
            else sql_file.read()
        )
    query_job = bq_client.query(parsed_sql, job_config=_JOB_CONFIG)
    logging.info(f"{file_name}: This query will process {query_job.total_bytes_processed} bytes.")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Validate sql files.")
    parser.add_argument(
        "files",
        metavar="F",
        nargs="+",
        help="SQL files to process",
    )

    args = parser.parse_args()

    client = bigquery.Client()
    logging.info(f"Testing {len(args.files)} SQL files.")
    for filename in args.files:
        validate_sql(filename, client)
        logging.info(f"{filename} OK!")
