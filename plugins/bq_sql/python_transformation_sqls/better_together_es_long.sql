-- fetch all `ES` rows from long tables
WITH src_table AS (
    SELECT
        submission_id,
        '_' || question_order || '_' AS question_order,
        question_type,
        response_label
    FROM `{{ params.table_name }}`
    WHERE `language` = 'ES'
)
SELECT submission_id, question_order, STRING_AGG(response_label, '^') AS response_label
FROM src_table
GROUP BY submission_id, question_order
