SELECT
    submission_date,
    gender,
    age,
    `geography`,
    financial_situation,
    education,
    employment_status,
    submission_state,
    submission_municipality,
    submission_parish,
    SUM(hdds) AS hdds_sum,
    SUM(CASE WHEN hdds >= 6 THEN 1 ELSE 0 END) AS hdds_high_count,
    SUM(CASE WHEN hdds >= 4 AND hdds < 6 THEN 1 ELSE 0 END) AS hdds_medium_count,
    SUM(CASE WHEN hdds < 4 THEN 1 ELSE 0 END) AS hdds_low_count,
    SUM(fcs) AS fcs_sum,
    COUNT(1) as group_count
FROM `{{ params.src_table }}`
GROUP BY
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10
