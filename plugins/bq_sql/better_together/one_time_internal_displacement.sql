CREATE TEMPORARY FUNCTION yesNoToInt(x STRING)
RETURNS INT64
AS (
  CASE x
    WHEN 'Yes' THEN 1
    WHEN 'No' THEN 0
    ELSE NULL
  END
);

WITH source_table AS (
  SELECT *
  FROM `{{ params.src_table }}`
  WHERE L0_name = 'Venezuela'
), vzla_loc AS (
  SELECT
    t.submission_id,
    ARRAY_AGG(
      STRUCT(
        l.adm1_es AS submission_state,
        l.adm2_es AS submission_municipality,
        l.adm3_es AS submission_parish
      )
      ORDER BY ST_DISTANCE(
        ST_GEOGPOINT(t.observation_lon, t.observation_lat),
        l.location
      )
      LIMIT 1
    )[SAFE_OFFSET(0)] as loc
  FROM source_table t, `better_together_raw.venezuela_boundaries` l
  WHERE ST_DWITHIN(ST_GEOGPOINT(t.observation_lon, t.observation_lat), l.location, 100000)
  GROUP BY t.submission_id
), location_enriched AS (  -- append location, grab subset of desired columns, and generate new simple ones
  SELECT lt.submission_id
      , observed_time AS submission_timestamp
      , DATE(observed_time) AS submission_date
      , `user_id`
      , gender
      , age
      , geography
      , financial_situation
      , education
      , employment_status
      , vl.loc.submission_state
      , vl.loc.submission_municipality
      , vl.loc.submission_parish
      , _1_ AS reported_state_of_residence
      , _2_ AS how_long_have_lived_in_reported_state_of_residence
      , _3_ AS reported_previous_state_of_residence
      , _4_ AS main_reason_for_decision_to_move
      , _5_ AS how_long_plans_to_stay_in_current_location
      , _6_ AS how_long_traveled_when_moved
      , _7_ AS who_else_traveled_when_moved
      , SAFE_CAST(_8_ AS INT64) AS how_many_people_traveled_when_moved
      , _9_ AS method_of_travel_when_moved
      , _10_ AS main_reason_for_selected_new_location
      , yesNoToInt(_11_) AS new_location_meets_expectations
      , CASE _12_ WHEN 'Temporarily' THEN 1 ELSE 0 END AS plans_to_stay_in_current_location_temporarily
      , _13_ AS next_destination
      , yesNoToInt(_14_) AS has_place_to_go_at_final_destination
      , _15_ AS main_reason_for_decision_to_move_again
      , _16_ AS type_of_help_needed_to_get_to_final_destination
  FROM source_table AS lt
  LEFT JOIN vzla_loc vl ON lt.submission_id = vl.submission_id
)
SELECT *
FROM location_enriched
