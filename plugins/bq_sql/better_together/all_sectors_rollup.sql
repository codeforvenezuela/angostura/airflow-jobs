WITH agg_tbl AS (
  SELECT 'daily_food' AS datasource
    , submission_date
    , submission_state
    , submission_municipality
    , submission_parish
    , gender
    , age
    , geography
    , financial_situation
    , education
    , employment_status
    , COUNT(*) AS count
  FROM `{{ params.daily_food_table }}`
  GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11
  UNION ALL
  SELECT 'weekly_food' AS datasource
    , submission_date
    , submission_state
    , submission_municipality
    , submission_parish
    , gender
    , age
    , geography
    , financial_situation
    , education
    , employment_status
    , COUNT(*) AS count
  FROM `{{ params.weekly_food_table }}`
  GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11
  UNION ALL
  SELECT 'monthly_health' AS datasource
    , submission_date
    , submission_state
    , submission_municipality
    , submission_parish
    , gender
    , age
    , geography
    , financial_situation
    , education
    , employment_status
    , COUNT(*) AS count
  FROM `{{ params.monthly_health_table }}`
  GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11)
SELECT *
FROM agg_tbl
