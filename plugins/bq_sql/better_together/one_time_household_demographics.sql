CREATE TEMPORARY FUNCTION yesNoToInt(x STRING)
RETURNS INT64
AS (
  CASE x
    WHEN 'Yes' THEN 1
    WHEN 'No' THEN 0
    ELSE NULL
  END
);

WITH source_table AS (
  SELECT *
  FROM `{{ params.src_table }}`
  WHERE L0_name = 'Venezuela'
), vzla_loc AS (
  SELECT
    t.submission_id,
    ARRAY_AGG(
      STRUCT(
        l.adm1_es AS submission_state,
        l.adm2_es AS submission_municipality,
        l.adm3_es AS submission_parish
      )
      ORDER BY ST_DISTANCE(
        ST_GEOGPOINT(t.observation_lon, t.observation_lat),
        l.location
      )
      LIMIT 1
    )[SAFE_OFFSET(0)] as loc
  FROM source_table t, `better_together_raw.venezuela_boundaries` l
  WHERE ST_DWITHIN(ST_GEOGPOINT(t.observation_lon, t.observation_lat), l.location, 100000)
  GROUP BY t.submission_id
), location_enriched AS (  -- append location, grab subset of desired columns, and generate new simple ones
  SELECT lt.submission_id
      , observed_time AS submission_timestamp
      , DATE(observed_time) AS submission_date
      , `user_id`
      , gender
      , age
      , geography
      , financial_situation
      , education
      , employment_status
      , vl.loc.submission_state
      , vl.loc.submission_municipality
      , vl.loc.submission_parish
      , _1_ AS how_many_people_live_in_the_household
      , _2_ AS how_many_people_ages_0_to_4_live_in_the_household
      , _3_ AS how_many_girls_ages_0_to_4_live_in_the_household
      , _4_ AS how_many_people_ages_5_to_13_live_in_the_household
      , _5_ AS how_many_girls_ages_5_to_13_live_in_the_household
      , _6_ AS how_many_people_ages_14_to_17_live_in_the_household
      , _7_ AS how_many_girls_ages_14_to_17_live_in_the_household
      , _8_ AS how_many_people_ages_18_to_49_live_in_the_household
      , _9_ AS how_many_females_ages_18to_49_live_in_the_household
      , _10_ AS how_many_people_ages_50_to_64_live_in_the_household
      , _11_ AS how_many_females_ages_50_to_64_live_in_the_household
      , _12_ AS how_many_people_ages_65_live_in_the_household
      , _13_ AS how_many_females_ages_65_live_in_the_household
      , _14_ AS how_many_people_in_the_household_have_a_disability
      , yesNoToInt(_15_) AS is_there_more_than_one_family_group_in_the_household
      , _16_ AS what_is_the_type_of_housing_where_family_lives
      , _17_ AS what_is_the_other_type_of_housing_where_family_lives
      , yesNoToInt(_18_) AS is_head_of_the_household
      , _19_ AS what_is_the_current_position_of_head_of_the_household
      , _20_ AS what_is_the_position_sector_of_head_of_the_household
      , _21_ AS what_is_the_country_where_respondant_lives
      , _22_ AS what_is_the_country_where_respondant_works
      , _23_ AS how_many_people_work_to_help_support_your_household
      , _24_ AS what_is_the_monthly_income_of_your_household
      , _25_ AS how_often_primary_income_is_received
      , _26_ AS which_activities_are_performed_in_your_household_for_income
      , _27_ AS which_other_activities_are_performed_in_your_household_for_income
      , _28_ AS how_often_is_government_bonuses_or_assistance_received
      , _29_ AS which_options_best_describe_remote_work
      , _30_ AS which_other_option_best_describe_remote_work
      , _31_ AS what_is_the_target_audience_of_multimedia_content
      , _32_ AS how_often_are_remittances_or_aid_from_abroad_received
      , _33_ AS what_type_of_aid_or_remittances_from_abroad_is_received
      , _34_ AS how_many_approximate_dollars_are_received_from_abroad
      , _35_ AS what_is_the_financial_situation_today_like
      , _36_ AS what_is_the_financial_situation_today_compared_to_6_months_ago
      , _37_ AS how_much_is_spent_monthly_on_food_purchases_for_the_household
      , _38_ AS which_goods_or_services_are_present_in_the_household
      , _39_ AS how_frequently_is_water_service_available_in_the_household
      , _40_ AS what_types_of_transportation_are_most_frequently_used_in_the_household
      , _41_ AS what_other_type_of_transportation_is_most_frequently_used_in_the_household
      , _42_ AS what_physical_or_material_impact_has_the_crisis_represented_in_the_household
      , _43_ AS what_is_the_biggest_blocker_currently_faced_to_execute_work_activities
      , _44_ AS what_is_the_other_blocker_currently_faced_to_execute_work_activities
      , _45_ AS which_responsibilities_would_be_concerned_to_not_fulfill_if_called_into_lockdown_due_to_covid19
      , _46_ AS which_other_responsibility_would_be_concerned_to_not_fulfill_if_called_into_lockdown_due_to_covid19
      , _47_ AS which_benefits_are_received_from_employment
      , _48_ AS which_other_benefit_is_received_from_employment
      , _49_ AS how_are_wages_or_payments_customarily_received_in_the_household
      , _50_ AS what_other_way_is_wages_or_payments_customarily_received_in_the_household
      , _51_ AS what_types_of_financial_difficulties_are_encountered_when_receiving_salary
  FROM source_table AS lt
  LEFT JOIN vzla_loc vl ON lt.submission_id = vl.submission_id
)
SELECT *
FROM location_enriched
