CREATE TEMPORARY FUNCTION yesNoToInt(x STRING)
RETURNS INT64
AS (
  CASE x
    WHEN 'Yes' THEN 1
    WHEN 'No' THEN 0
    ELSE NULL
  END
);

WITH source_table AS (
  SELECT *
  FROM `{{ params.src_table }}`
  WHERE L0_name = 'Venezuela'
), vzla_loc AS (
  SELECT
    t.submission_id,
    ARRAY_AGG(
      STRUCT(
        l.adm1_es AS submission_state,
        l.adm2_es AS submission_municipality,
        l.adm3_es AS submission_parish
      )
      ORDER BY ST_DISTANCE(
        ST_GEOGPOINT(t.observation_lon, t.observation_lat),
        l.location
      )
      LIMIT 1
    )[SAFE_OFFSET(0)] as loc
  FROM source_table t, `better_together_raw.venezuela_boundaries` l
  WHERE ST_DWITHIN(ST_GEOGPOINT(t.observation_lon, t.observation_lat), l.location, 100000)
  GROUP BY t.submission_id
), location_enriched AS (  -- append location, grab subset of desired columns, and generate new simple ones
  SELECT lt.submission_id
      , observed_time AS submission_timestamp
      , DATE(observed_time) AS submission_date
      , `user_id`
      , gender
      , age
      , geography
      , financial_situation
      , education
      , employment_status
      , vl.loc.submission_state
      , vl.loc.submission_municipality
      , vl.loc.submission_parish
      , yesNoToInt(_4_) AS are_there_children_0_to_2_yrs_out_of_educational_system
      , yesNoToInt(_5_) AS were_children_3_to_17_yrs_enrolled_and_did_not_return_to_school
      , yesNoToInt(_6_) AS are_there_children_who_stopped_enrolling_in_primary_education
      , yesNoToInt(_7_) AS are_there_children_who_stopped_enrolling_in_secondary_education
      , yesNoToInt(_8_) AS are_children_attending_face_to_face_classes
      , yesNoToInt(_9_) AS can_children_observe_deterioration_of_basic_services_of_school
      , _10_ AS do_children_3_and_17_yrs_receive_regular_school_meals
      , _11_ AS are_there_teachers_at_scheduled_class_hours
      , yesNoToInt(_12_) AS are_children_3_to_17_yrs_dealing_with_irregular_school_activity
      , yesNoToInt(_13_) AS are_children_being_teached_by_unqualified_people
      , yesNoToInt(_14_) AS did_teachers_leave_the_educational_system
      , yesNoToInt(_15_) AS do_school_and_the_teachers_have_internet_connection
      , yesNoToInt(_16_) AS do_children_have_internet_connection
      , yesNoToInt(_17_) AS do_children_3_to_17_yrs_miss_virtual_class_due_to_lack_of_electricity
      , yesNoToInt(_18_) AS does_home_shows_severe_deficit_of_electricity
      , yesNoToInt(_19_) AS does_home_shows_severe_deficit_of_internet
      , yesNoToInt(_20_) AS do_children_3_to_17_yrs_miss_class_or_in_lower_grade
      , yesNoToInt(_21_) AS are_children_promoted_with_a_modality_different_from_formal_evaluation
  FROM source_table AS lt
  LEFT JOIN vzla_loc vl ON lt.submission_id = vl.submission_id
)
SELECT *
FROM location_enriched
