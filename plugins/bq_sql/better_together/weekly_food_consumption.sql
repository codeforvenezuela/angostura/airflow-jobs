CREATE TEMPORARY FUNCTION daysFromString(x STRING)
RETURNS INT64
AS (
  CASE x
    WHEN 'Never' THEN 0
    ELSE CAST(SPLIT(x, ' ')[OFFSET(0)] AS INT64)
  END
);

WITH source_table as (
  SELECT *
  FROM `{{ params.src_table }}`
  WHERE L0_name = 'Venezuela'
), vzla_loc AS (
  SELECT
    t.submission_id,
    ARRAY_AGG(
      STRUCT(
        l.adm1_es AS submission_state,
        l.adm2_es AS submission_municipality,
        l.adm3_es AS submission_parish
      )
      ORDER BY ST_DISTANCE(
        ST_GEOGPOINT(t.observation_lon, t.observation_lat),
        l.location
      )
      LIMIT 1
    )[SAFE_OFFSET(0)] as loc
  FROM source_table t, `better_together_raw.venezuela_boundaries` l
  WHERE ST_DWITHIN(ST_GEOGPOINT(t.observation_lon, t.observation_lat), l.location, 100000)
  GROUP BY t.submission_id
), location_enriched AS (  -- append location to table
  SELECT lt.*
      , vl.loc.submission_state
      , vl.loc.submission_municipality
      , vl.loc.submission_parish
  FROM source_table AS lt
  LEFT JOIN vzla_loc vl ON lt.submission_id = vl.submission_id
), renamed_cols AS ( -- rename long column names for easier handling
  SELECT *
    , _1_ AS cereals_plantains_tubers_7days
    , _2_ AS grains_7days
    , _3_ AS vegs_7days
    , _4_ AS fruits_7days
    , _5_ AS meat_eggs_seafood_7days
    , _6_ AS dairy_7days
    , _7_ AS oils_fats_7days
    , _8_ AS sugars_7days
    , _9_ AS cheap_food_7days
    , _10_ AS skip_food_7days
    , _11_ AS portion_reduction_7days
    , _12_ AS adult_reduction_7days
    , _13_ AS eaten_one_time_or_not_eaten_7days
    , _14_ AS non_adult_sent_to_eat_somewhere_else_7days
    , _15_ AS adult_sent_to_eat_somewhere_else_7days
    , _16_ AS borrowed_food_7days
    , _17_ AS help_from_others_in_country_7days
    , _18_ AS help_from_others_outside_country_7days
    , _19_ AS spend_food_savings_7days
    , _20_ AS sell_or_exchange_property_7days
    , _21_ AS work_in_exchange_for_food_7days
    , _22_ AS food_purchased_7days
    , _23_ AS food_purchased_from_7days
    , _24_ AS food_transferred_7days
    , _25_ AS avg_distance_traveled_for_food_7days
    , _26_ AS head_household
  FROM location_enriched
), final_rich AS ( -- grab subset of desired columns, clean some of them, and generate new simple ones
  SELECT submission_id
    , observed_time AS submission_timestamp
    , DATE(observed_time) AS submission_date
    , `user_id`
    , gender
    , age
    , geography
    , financial_situation
    , education
    , employment_status
    , submission_state
    , submission_municipality
    , submission_parish
    , daysFromString(cereals_plantains_tubers_7days) AS cereals_plantains_tubers_7days
    , daysFromString(vegs_7days) AS vegs_7days
    , daysFromString(fruits_7days) AS fruits_7days
    , daysFromString(meat_eggs_seafood_7days) AS meat_eggs_seafood_7days
    , daysFromString(grains_7days) AS grains_7days
    , daysFromString(dairy_7days) AS dairy_7days
    , daysFromString(oils_fats_7days) AS oils_fats_7days
    , daysFromString(sugars_7days) AS sugars_7days
    , daysFromString(cheap_food_7days) AS cheap_food_7days
    , daysFromString(skip_food_7days) AS skip_food_7days
    , daysFromString(portion_reduction_7days) AS portion_reduction_7days
    , daysFromString(adult_reduction_7days) AS adult_reduction_7days
    , daysFromString(eaten_one_time_or_not_eaten_7days) AS eaten_one_time_or_not_eaten_7days
    , daysFromString(non_adult_sent_to_eat_somewhere_else_7days) AS non_adult_sent_to_eat_somewhere_else_7days
    , daysFromString(adult_sent_to_eat_somewhere_else_7days) AS adult_sent_to_eat_somewhere_else_7days
    , daysFromString(borrowed_food_7days) AS borrowed_food_7days
    , daysFromString(help_from_others_in_country_7days) AS help_from_others_in_country_7days
    , daysFromString(help_from_others_outside_country_7days) AS help_from_others_outside_country_7days
    , daysFromString(spend_food_savings_7days) AS spend_food_savings_7days
    , daysFromString(sell_or_exchange_property_7days) AS sell_or_exchange_property_7days
    , daysFromString(work_in_exchange_for_food_7days) AS work_in_exchange_for_food_7days
    , daysFromString(food_purchased_7days) AS food_purchased_7days
    , food_purchased_from_7days
    , food_transferred_7days
    , avg_distance_traveled_for_food_7days
    , head_household
  FROM renamed_cols
)
SELECT *
FROM final_rich
