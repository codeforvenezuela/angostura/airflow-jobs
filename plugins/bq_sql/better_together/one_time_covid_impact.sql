CREATE TEMPORARY FUNCTION yesNoToInt(x STRING)
RETURNS INT64
AS (
  CASE x
    WHEN 'Yes' THEN 1
    WHEN 'No' THEN 0
    ELSE NULL
  END
);

WITH source_table AS (
  SELECT *
  FROM `{{ params.src_table }}`
  WHERE L0_name = 'Venezuela'
), vzla_loc AS (
  SELECT
    t.submission_id,
    ARRAY_AGG(
      STRUCT(
        l.adm1_es AS submission_state,
        l.adm2_es AS submission_municipality,
        l.adm3_es AS submission_parish
      )
      ORDER BY ST_DISTANCE(
        ST_GEOGPOINT(t.observation_lon, t.observation_lat),
        l.location
      )
      LIMIT 1
    )[SAFE_OFFSET(0)] as loc
  FROM source_table t, `better_together_raw.venezuela_boundaries` l
  WHERE ST_DWITHIN(ST_GEOGPOINT(t.observation_lon, t.observation_lat), l.location, 100000)
  GROUP BY t.submission_id
), location_enriched AS (  -- append location, grab subset of desired columns, and generate new simple ones
  SELECT lt.submission_id
      , observed_time AS submission_timestamp
      , DATE(observed_time) AS submission_date
      , `user_id`
      , gender
      , age
      , geography
      , financial_situation
      , education
      , employment_status
      , vl.loc.submission_state
      , vl.loc.submission_municipality
      , vl.loc.submission_parish
      , yesNoToInt(_2_) AS presented_symptoms_of_covid_19_in_last_8days
      , yesNoToInt(_3_) AS knows_where_to_go_for_covid_19_test
      , _4_ AS preferred_centers_for_covid_19_testing
      , _5_ AS walking_distance_to_closest_covid_19_testing_center
      , yesNoToInt(_6_) AS have_visited_covid_19_test_center
      , _7_ AS type_of_covid_19_test_taken
      , yesNoToInt(_8_) AS covid_19_test_taken_was_positive
      , _9_ AS covid_19_test_cost
      , _10_ AS agreement_feeling_towards_covid_19_test_cost
      , yesNoToInt(_11_) AS reports_movement_restrictions_due_to_covid_19
      , yesNoToInt(_12_) AS reports_negative_community_impact_due_to_covid_19
      , _13_ AS agreement_feeling_towards_covid_19_impacting_income
      , _14_ AS how_likely_to_social_distance_when_out_in_public
      , _15_ AS reported_state_of_residence
      , _16_ AS how_long_have_lived_in_reported_state_of_residence
      , yesNoToInt(_17_) AS covid_19_impacted_ability_to_move_from_last_residence
      , yesNoToInt(_18_) AS moved_as_result_of_covid_19
  FROM source_table AS lt
  LEFT JOIN vzla_loc vl ON lt.submission_id = vl.submission_id
)
SELECT *
FROM location_enriched
