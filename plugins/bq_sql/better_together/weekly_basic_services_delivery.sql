CREATE TEMPORARY FUNCTION yesNoToInt(x STRING)
RETURNS INT64
AS (
  CASE x
    WHEN 'Yes' THEN 1
    WHEN 'No' THEN 0
    ELSE NULL
  END
);

CREATE TEMPORARY FUNCTION daysFromString(x STRING)
RETURNS INT64
AS (
  CASE x
    WHEN 'Never' THEN 0
    ELSE CAST(SPLIT(x, ' ')[OFFSET(0)] AS INT64)
  END
);

WITH source_table AS (
  SELECT *
  FROM `{{ params.src_table }}`
  WHERE L0_name = 'Venezuela'
), vzla_loc AS (
  SELECT
    t.submission_id,
    ARRAY_AGG(
      STRUCT(
        l.adm1_es AS submission_state,
        l.adm2_es AS submission_municipality,
        l.adm3_es AS submission_parish
      )
      ORDER BY ST_DISTANCE(
        ST_GEOGPOINT(t.observation_lon, t.observation_lat),
        l.location
      )
      LIMIT 1
    )[SAFE_OFFSET(0)] as loc
  FROM source_table t, `better_together_raw.venezuela_boundaries` l
  WHERE ST_DWITHIN(ST_GEOGPOINT(t.observation_lon, t.observation_lat), l.location, 100000)
  GROUP BY t.submission_id
), location_enriched AS (  -- append location, grab subset of desired columns, and generate new simple ones
  SELECT lt.submission_id
      , observed_time AS submission_timestamp
      , DATE(observed_time) AS submission_date
      , `user_id`
      , gender
      , age
      , geography
      , financial_situation
      , education
      , employment_status
      , vl.loc.submission_state
      , vl.loc.submission_municipality
      , vl.loc.submission_parish
      , yesNoToInt(_1_) AS uninterrupted_electricity_24hrs
      , daysFromString(_2_) AS uninterrupted_electricity_7days
      , SAFE_CAST(_3_ AS INT64) AS hours_interrupted_electricity_7days
      , yesNoToInt(_4_) AS have_full_gas_cylinder
      , _5_ AS frquency_service_for_changing_gas_cylinders
      , yesNoToInt(_6_) AS have_place_to_get_gas_cylinders_near_house
      , yesNoToInt(_7_) AS used_other_means_of_cooking_because_lack_of_gas_cylinder
      , _8_ AS frequency_of_garbage_collection_last_month
      , _9_ AS method_for_waste_disposal_when_no_garbage_collection
  FROM source_table AS lt
  LEFT JOIN vzla_loc vl ON lt.submission_id = vl.submission_id
)
SELECT *
FROM location_enriched
