CREATE TEMPORARY FUNCTION yesNoToInt(x STRING)
RETURNS INT64
AS (
  CASE x
    WHEN 'Yes' THEN 1
    WHEN 'No' THEN 0
    ELSE NULL
  END
);

WITH source_table AS (
  SELECT *
  FROM `{{ params.src_table }}`
  WHERE L0_name = 'Venezuela'
), vzla_loc AS (
  SELECT
    t.submission_id,
    ARRAY_AGG(
      STRUCT(
        l.adm1_es AS submission_state,
        l.adm2_es AS submission_municipality,
        l.adm3_es AS submission_parish
      )
      ORDER BY ST_DISTANCE(
        ST_GEOGPOINT(t.observation_lon, t.observation_lat),
        l.location
      )
      LIMIT 1
    )[SAFE_OFFSET(0)] as loc
  FROM source_table t, `better_together_raw.venezuela_boundaries` l
  WHERE ST_DWITHIN(ST_GEOGPOINT(t.observation_lon, t.observation_lat), l.location, 100000)
  GROUP BY t.submission_id
), location_enriched AS (  -- append location, grab subset of desired columns, and generate new simple ones
  SELECT lt.submission_id
      , observed_time AS submission_timestamp
      , DATE(observed_time) AS submission_date
      , `user_id`
      , _1_ AS gender
      , _2_ AS age
      , geography
      , financial_situation
      , education
      , employment_status
      , vl.loc.submission_state
      , vl.loc.submission_municipality
      , vl.loc.submission_parish
      , yesNoToInt(_3_) AS visited_center_last_3months
      , _4_ AS type_of_visited_center
      , _5_ AS other_type_of_visited_center
      , _6_ AS reason_of_visit
      , _7_ AS other_reason_of_visit
      , yesNoToInt(_8_) AS did_receive_medical_attention_during_visit
      , _9_ AS reason_for_no_attention_during_visit
      , _10_ AS other_reason_for_no_attention_during_visit
      , _11_ AS who_attended_visit
      , _12_ AS other_who_attended_visit
      , yesNoToInt(_13_) AS was_asked_to_take_medication
      , yesNoToInt(_14_) AS received_prescription
      , _15_ AS medication_source
      , _16_ AS other_medication_source
      , _17_ AS reason_for_not_getting_medication
      , SAFE_CAST(_18_ AS INT64) AS supply_stores_visited
      , SAFE_CAST(_19_ AS INT64) AS avg_days_invested_on_medication_search
      , _20_ AS mode_of_transport_for_medication
      , _21_ AS other_mode_of_transport_for_medication
      , yesNoToInt(_22_) AS someone_has_chronic_health_conditions_requiring_medicine
      , _23_ AS type_of_chronic_health_conditions_requiring_medicine
      , _24_ AS other_type_of_chronic_health_conditions_requiring_medicine
      , yesNoToInt(_25_) AS takes_medicine_for_chronic_health_conditions
      , SAFE_CAST(_26_ AS INT64) AS monthly_hypertension_medicine_units_needed
      , yesNoToInt(_27_) AS has_limit_to_medications_amount
      , _28_ AS medications_supply_length
      , _29_ AS reason_for_3months_medications_supply
      , _30_ AS other_reason_for_3months_medications_supply
      , yesNoToInt(_31_) AS regular_access_to_medications
      , _32_ AS reason_for_not_taking_medication
      , _33_ AS currency_for_buying_medications
      , _34_ AS other_currency_for_buying_medications
      , _35_ AS avg_monthly_spent_on_medications_or_attention
      , yesNoToInt(_36_) AS noticed_increse_in_medications_price
      , _37_ AS amount_increse_in_medications_price
  FROM source_table AS lt
  LEFT JOIN vzla_loc vl ON lt.submission_id = vl.submission_id
)
SELECT *
FROM location_enriched
