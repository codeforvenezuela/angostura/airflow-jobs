WITH source_table AS (
  SELECT *
  FROM `{{ params.src_table }}`
  WHERE L0_name = 'Venezuela'
), vzla_loc AS (
  SELECT
    t.submission_id,
    ARRAY_AGG(
      STRUCT(
        l.adm1_es AS submission_state,
        l.adm2_es AS submission_municipality,
        l.adm3_es AS submission_parish
      )
      ORDER BY ST_DISTANCE(
        ST_GEOGPOINT(t.observation_lon, t.observation_lat),
        l.location
      )
      LIMIT 1
    )[SAFE_OFFSET(0)] as loc
  FROM source_table t, `better_together_raw.venezuela_boundaries` l
  WHERE ST_DWITHIN(ST_GEOGPOINT(t.observation_lon, t.observation_lat), l.location, 100000)
  GROUP BY t.submission_id
), location_enriched AS (  -- append location, grab subset of desired columns, and generate new simple ones
  SELECT lt.submission_id
      , observed_time AS submission_timestamp
      , DATE(observed_time) AS submission_date
      , `user_id`
      , gender
      , age
      , geography
      , financial_situation
      , education
      , employment_status
      , vl.loc.submission_state
      , vl.loc.submission_municipality
      , vl.loc.submission_parish
      , _1_ AS who_is_the_head_of_household
      , _2_ AS what_is_the_employment_situation_of_the_head_of_household
      , _3_ AS what_is_the_situation_currently_in_your_community
      , _4_ AS description_of_current_economic_situation
      , _5_ AS situation_compared_with_3_months_ago
      , _6_ AS what_is_the_monthly_income_of_family
      , _7_ AS how_much_does_household_spends_monthly_on_buying_food
      , _8_ AS has_someone_in_family_moved_outside_of_venezuela
      , SAFE_CAST(_9_ AS INT64) AS how_many_in_family_moved_outside_of_venezuela
      , _10_ AS has_someone_in_family_outside_of_venezuela_helped_financially
      , _11_ AS dollars_in_financial_aid_from_family_outside_of_venezuela
      , _12_ AS portion_of_financial_aid_from_family_outside_of_venezuela_spent_on_food
      , _13_ AS does_someone_in_household_receives_government_aid
      , _14_ AS how_often_someone_in_household_receives_government_aid
      , _15_ AS portion_government_aid_spent_on_food
      , _16_ AS had_opportunity_to_purchase_clap_food_boxes
      , _17_ AS how_often_have_received_clap_food_boxes
      , _18_ AS opinion_on_clap_boxes_containing_enough_food_for_family
      , _19_ AS have_received_food_in_poor_condition_in_clap_food_boxes
  FROM source_table AS lt
  LEFT JOIN vzla_loc vl ON lt.submission_id = vl.submission_id
)
SELECT *
FROM location_enriched
