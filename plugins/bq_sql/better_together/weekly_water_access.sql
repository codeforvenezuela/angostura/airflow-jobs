CREATE TEMPORARY FUNCTION yesNoToInt(x STRING)
RETURNS INT64
AS (
  CASE x
    WHEN 'Yes' THEN 1
    WHEN 'No' THEN 0
    ELSE NULL
  END
);

CREATE TEMPORARY FUNCTION daysFromString(x STRING)
RETURNS INT64
AS (
  CASE x
    WHEN 'Never' THEN 0
    ELSE CAST(SPLIT(x, ' ')[OFFSET(0)] AS INT64)
  END
);

WITH source_table_v1 AS (
  SELECT *
  FROM `{{ params.src_table_v1 }}`
  WHERE L0_name = 'Venezuela'
), source_table_v2 AS (
  SELECT *
  FROM `{{ params.src_table_v2 }}`
  WHERE L0_name = 'Venezuela'
), renamed_v1 AS (
  SELECT submission_id
    , observed_time
    , `user_id`
    , gender
    , age
    , geography
    , financial_situation
    , education
    , employment_status
    , campaign_name
    , observation_lon
    , observation_lat
    , _7_ AS amount_of_people_in_household
    , _8_ AS amount_of_females_in_household
    , _9_ AS amount_of_males_in_household
    , yesNoToInt(_10_) AS uninterrupted_water_24hrs
    , daysFromString(_11_) AS water_access_7days
    , daysFromString(_12_) AS water_from_public_access_point_7days
    , _13_ AS avg_time_to_get_water
    , _14_ AS avg_distance_to_get_water_7days
    , _15_ AS method_to_make_water_potable
    , SAFE_CAST(_16_ AS INT64) AS hours_of_water_at_home_per_day
    , _17_ AS frequency_water_at_home_last_month
    , _18_ AS primary_method_to_drinkable_water_in_area
  FROM source_table_v1
), renamed_v2 AS (
  SELECT submission_id
    , observed_time
    , `user_id`
    , gender
    , age
    , geography
    , financial_situation
    , education
    , employment_status
    , campaign_name
    , observation_lon
    , observation_lat
    , _3_ AS amount_of_people_in_household
    , _4_ AS amount_of_females_in_household
    , _5_ AS amount_of_males_in_household
    , yesNoToInt(_6_) AS uninterrupted_water_24hrs
    , daysFromString(_7_) AS water_access_7days
    , daysFromString(_8_) AS water_from_public_access_point_7days
    , _9_ AS avg_time_to_get_water
    , _10_ AS avg_distance_to_get_water_7days
    , _11_ AS method_to_make_water_potable
    , SAFE_CAST(_12_ AS INT64) AS hours_of_water_at_home_per_day
    , _13_ AS frequency_water_at_home_last_month
    , _14_ AS primary_method_to_drinkable_water_in_area
  FROM source_table_v2
), union_source AS ( -- union of versions with compatible schema
  SELECT *
  FROM renamed_v1
  UNION ALL
  SELECT *
  FROM renamed_v2
), vzla_loc AS (
  SELECT
    t.submission_id,
    ARRAY_AGG(
      STRUCT(
        l.adm1_es AS submission_state,
        l.adm2_es AS submission_municipality,
        l.adm3_es AS submission_parish
      )
      ORDER BY ST_DISTANCE(
        ST_GEOGPOINT(t.observation_lon, t.observation_lat),
        l.location
      )
      LIMIT 1
    )[SAFE_OFFSET(0)] as loc
  FROM union_source t, `better_together_raw.venezuela_boundaries` l
  WHERE ST_DWITHIN(ST_GEOGPOINT(t.observation_lon, t.observation_lat), l.location, 100000)
  GROUP BY t.submission_id
), location_enriched AS (  -- append location
  SELECT lt.submission_id
    , observed_time AS submission_timestamp
    , DATE(observed_time) AS submission_date
    , `user_id`
    , gender
    , age
    , geography
    , financial_situation
    , education
    , employment_status
    , vl.loc.submission_state
    , vl.loc.submission_municipality
    , vl.loc.submission_parish
    , amount_of_people_in_household
    , amount_of_females_in_household
    , amount_of_males_in_household
    , uninterrupted_water_24hrs
    , water_access_7days
    , water_from_public_access_point_7days
    , avg_time_to_get_water
    , avg_distance_to_get_water_7days
    , method_to_make_water_potable
    , hours_of_water_at_home_per_day
    , frequency_water_at_home_last_month
    , primary_method_to_drinkable_water_in_area
  FROM union_source AS lt
  LEFT JOIN vzla_loc vl ON lt.submission_id = vl.submission_id
)
SELECT *
FROM location_enriched
