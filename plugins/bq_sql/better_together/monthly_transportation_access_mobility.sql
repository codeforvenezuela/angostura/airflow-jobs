CREATE TEMPORARY FUNCTION yesNoToInt(x STRING)
RETURNS INT64
AS (
  CASE x
    WHEN 'Yes' THEN 1
    WHEN 'No' THEN 0
    ELSE NULL
  END
);

WITH source_table AS (
  SELECT *
  FROM `{{ params.src_table }}`
  WHERE L0_name = 'Venezuela'
), vzla_loc AS (
  SELECT
    t.submission_id,
    ARRAY_AGG(
      STRUCT(
        l.adm1_es AS submission_state,
        l.adm2_es AS submission_municipality,
        l.adm3_es AS submission_parish
      )
      ORDER BY ST_DISTANCE(
        ST_GEOGPOINT(t.observation_lon, t.observation_lat),
        l.location
      )
      LIMIT 1
    )[SAFE_OFFSET(0)] as loc
  FROM source_table t, `better_together_raw.venezuela_boundaries` l
  WHERE ST_DWITHIN(ST_GEOGPOINT(t.observation_lon, t.observation_lat), l.location, 100000)
  GROUP BY t.submission_id
), location_enriched AS (  -- append location, grab subset of desired columns, and generate new simple ones
  SELECT lt.submission_id
      , observed_time AS submission_timestamp
      , DATE(observed_time) AS submission_date
      , `user_id`
      , gender
      , age
      , _1_ geography
      , financial_situation
      , education
      , employment_status
      , vl.loc.submission_state
      , vl.loc.submission_municipality
      , vl.loc.submission_parish
      , _2_ AS method_to_take_public_transportation
      , _3_ AS motivation_for_travel
      , _4_ AS transportation_options
      , yesNoToInt(_5_) AS salvo_conductos_or_work_cards_respected
      , _6_ AS one_way_transportation_duration_between_home_and_work
      , _7_ AS roundtrip_transportation_cost_between_home_and_work
      , _8_ AS avg_distance_to_reach_public_transportation
      , _9_ AS feels_frequent_bus_service_near_house
      , _10_ AS feels_safe_traveling_in_buses
      , _11_ AS feels_buses_are_crowded
      , _12_ AS feels_bus_fare_is_fair
      , _13_ AS feels_safe_riding_motorcycle_taxi
      , _14_ AS feels_is_easy_to_find_motorcycle_taxi
      , _15_ AS feels_motorcycle_taxi_fare_is_fair
      , _16_ AS method_to_take_public_transportation_between_municipalities
      , _17_ AS motivation_for_travel_between_municipalities
      , yesNoToInt(_18_) AS salvo_conductos_or_work_cards_respected_between_municipalities
  FROM source_table AS lt
  LEFT JOIN vzla_loc vl ON lt.submission_id = vl.submission_id
)
SELECT *
FROM location_enriched
