CREATE TEMPORARY FUNCTION yesNoToInt(x STRING)
RETURNS INT64
AS (
  CASE x
    WHEN 'Yes' THEN 1
    WHEN 'No' THEN 0
    ELSE NULL
  END
);

WITH source_table AS (
  SELECT *
  FROM `{{ params.src_table }}`
  WHERE L0_name = 'Venezuela'
), vzla_loc AS (
  SELECT
    t.submission_id,
    ARRAY_AGG(
      STRUCT(
        l.adm1_es AS submission_state,
        l.adm2_es AS submission_municipality,
        l.adm3_es AS submission_parish
      )
      ORDER BY ST_DISTANCE(
        ST_GEOGPOINT(t.observation_lon, t.observation_lat),
        l.location
      )
      LIMIT 1
    )[SAFE_OFFSET(0)] as loc
  FROM source_table t, `better_together_raw.venezuela_boundaries` l
  WHERE ST_DWITHIN(ST_GEOGPOINT(t.observation_lon, t.observation_lat), l.location, 100000)
  GROUP BY t.submission_id
), location_enriched AS (  -- append location, grab subset of desired columns, and generate new simple ones
  SELECT lt.submission_id
      , observed_time AS submission_timestamp
      , DATE(observed_time) AS submission_date
      , `user_id`
      , gender
      , age
      , geography
      , financial_situation
      , education
      , employment_status
      , vl.loc.submission_state
      , vl.loc.submission_municipality
      , vl.loc.submission_parish
      , yesNoToInt(_1_) AS cereal_24hrs
      , yesNoToInt(_2_) AS plantains_24hrs
      , yesNoToInt(_3_) AS vegs_24hrs
      , yesNoToInt(_4_) AS fruits_24hrs
      , yesNoToInt(_5_) AS meat_24hrs
      , yesNoToInt(_6_) AS eggs_24hrs
      , yesNoToInt(_7_) AS fish_24hrs
      , yesNoToInt(_8_) AS grains_24hrs
      , yesNoToInt(_9_) AS dairy_24hrs
      , yesNoToInt(_10_) AS oils_fats_24hrs
      , yesNoToInt(_11_) AS sugars_24hrs
      , _12_ AS head_household
  FROM source_table lt
  LEFT JOIN vzla_loc vl ON lt.submission_id = vl.submission_id
), window_aggs_metrics_enriched AS ( -- run window aggregations
  SELECT *
    , cereal_24hrs + plantains_24hrs + vegs_24hrs + fruits_24hrs + meat_24hrs +
        eggs_24hrs + fish_24hrs + grains_24hrs + dairy_24hrs + oils_fats_24hrs +
        sugars_24hrs as hdds
    , CAST((CAST(cereal_24hrs AS BOOL) OR CAST(plantains_24hrs AS BOOL)) AS INT64) * 2 +
        vegs_24hrs + fruits_24hrs +
        CAST((CAST(meat_24hrs AS BOOL) OR CAST(eggs_24hrs AS BOOL) OR CAST(fish_24hrs AS BOOL)) AS INT64) * 4 +
        grains_24hrs * 3 + dairy_24hrs * 4 + oils_fats_24hrs * 0.5 +
        sugars_24hrs * 0.5 as fcs
  FROM location_enriched
)
SELECT *
FROM window_aggs_metrics_enriched
