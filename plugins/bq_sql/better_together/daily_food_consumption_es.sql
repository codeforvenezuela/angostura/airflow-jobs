WITH source_table AS (
  SELECT * FROM `{{ params.src_table }}`
), premise_translations AS (
    SELECT * FROM `event-pipeline.better_together_raw.premise_contributor_profile`
)
SELECT submission_id AS id_de_envio
    , submission_timestamp AS tiempo_de_envio
    , submission_date AS fecha_de_envio
    , `user_id` AS id_de_usuario
    , gender_j.inputs_es AS sexo
    , age_j.inputs_es AS edad
    , geography_j.inputs_es AS geografia
    , financial_j.inputs_es AS situacion_financiera
    , education_j.inputs_es AS educacion
    , employment_status_j.inputs_es AS situacion_empleo
    , submission_state AS entidad_federal_de_envio
    , submission_municipality AS municipio_de_envio
    , submission_parish AS parroquia_de_envio
    , cereal_24hrs AS cereal_24hrs
    , plantains_24hrs AS platanos_24hrs
    , vegs_24hrs AS vegetales_24hrs
    , fruits_24hrs AS frutas_24hrs
    , meat_24hrs AS carnes_24hrs
    , eggs_24hrs AS huevos_24hrs
    , fish_24hrs AS pescados_24hrs
    , grains_24hrs AS granos_24hrs
    , dairy_24hrs AS lacteos_24hrs
    , oils_fats_24hrs AS aceites_grasas_24hrs
    , sugars_24hrs AS azucares_24hrs
    , CASE
        WHEN head_household = 'Yes' THEN 1
        WHEN head_household = 'No' THEN 0
        ELSE NULL
      END AS jefe_de_casa
    , hdds
    , fcs
FROM source_table src
LEFT JOIN premise_translations gender_j ON gender_j.inputs_en = src.gender
  AND gender_j.column='gender'
LEFT JOIN premise_translations age_j ON age_j.inputs_en = src.age
  AND age_j.column='age'
LEFT JOIN premise_translations geography_j ON geography_j.inputs_en = src.geography
  AND geography_j.column='geography'
LEFT JOIN premise_translations financial_j ON financial_j.inputs_en = src.financial_situation
  AND financial_j.column='financial_situation'
LEFT JOIN premise_translations education_j ON education_j.inputs_en = src.education
  AND education_j.column='education'
LEFT JOIN premise_translations employment_status_j ON employment_status_j.inputs_en = src.employment_status
  AND employment_status_j.column='employment_status'
