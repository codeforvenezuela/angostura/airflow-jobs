CREATE TEMPORARY FUNCTION yesNoToInt(x STRING)
RETURNS INT64
AS (
  CASE x
    WHEN 'Yes' THEN 1
    WHEN 'No' THEN 0
    ELSE NULL
  END
);

WITH source_table AS (
  SELECT *
  FROM `{{ params.src_table }}`
  WHERE L0_name = 'Venezuela'
), vzla_loc AS (
  SELECT
    t.submission_id,
    ARRAY_AGG(
      STRUCT(
        l.adm1_es AS submission_state,
        l.adm2_es AS submission_municipality,
        l.adm3_es AS submission_parish
      )
      ORDER BY ST_DISTANCE(
        ST_GEOGPOINT(t.observation_lon, t.observation_lat),
        l.location
      )
      LIMIT 1
    )[SAFE_OFFSET(0)] as loc
  FROM source_table t, `better_together_raw.venezuela_boundaries` l
  WHERE ST_DWITHIN(ST_GEOGPOINT(t.observation_lon, t.observation_lat), l.location, 100000)
  GROUP BY t.submission_id
), location_enriched AS (  -- append location, grab subset of desired columns, and generate new simple ones
  SELECT lt.submission_id
      , observed_time AS submission_timestamp
      , DATE(observed_time) AS submission_date
      , `user_id`
      , gender
      , age
      , geography
      , financial_situation
      , education
      , employment_status
      , vl.loc.submission_state
      , vl.loc.submission_municipality
      , vl.loc.submission_parish
      , _2_ AS what_is_the_education_level_of_children_ages_3_to_17_in_the_household
      , _3_ AS how_many_children_ages_0_to_2_live_in_the_household
      , _4_ AS how_many_children_ages_3_to_5_live_in_the_household
      , _5_ AS how_many_children_ages_6_to_11_live_in_the_household
      , _6_ AS how_many_children_ages_12_to_17_live_in_the_household
      , yesNoToInt(_7_) AS is_there_any_children_in_the_household_that_enrolled_last_year_but_left_country
      , _8_ AS is_there_any_children_in_the_household_that_enrolled_last_year_and_did_not_return
      , yesNoToInt(_9_) AS is_there_any_children_in_the_household_that_did_not_enroll_in_last_pre_school_year
      , yesNoToInt(_10_) AS is_there_any_children_in_the_household_that_did_not_enroll_in_last_primary_school_year
      , yesNoToInt(_11_) AS is_there_any_children_in_the_household_that_did_not_enroll_in_last_secondary_school_year
      , _12_ AS is_the_children_that_did_not_enroll_in_last_school_year_still_in_the_country
      , _13_ AS how_are_the_children_in_the_household_attending_the_current_school_year
      , _14_ AS how_often_do_children_in_the_household_attending_the_current_school_year_receive_food
      , _15_ AS does_the_absence_of_school_provided_food_influence_the_decision_to_send_children_to_school
      , _16_ AS how_was_the_quality_of_school_provided_food_described
      , _17_ AS how_was_the_quantity_of_school_provided_food_described
      , _18_ AS how_was_the_variety_of_school_provided_food_described
      , _19_ AS are_the_teachers_available_during_the_determined_class_hours_in_current_school_year
      , _20_ AS why_are_the_teachers_not_available_during_the_determined_class_hours_in_current_school_year
      , _21_ AS why_else_are_the_teachers_not_available_during_the_determined_class_hours_in_current_school_year
      , _22_ AS what_size_is_the_classroom_of_the_children_in_the_household
      , _23_ AS how_many_students_are_in_the_classroom_during_a_school_day_in_current_school_year
      , _24_ AS how_many_students_is_one_teacher_responsible_for_during_classes_in_current_school_year
      , _25_ AS what_is_the_agreement_with_statement_number_of_students_in_the_classroom_permit_social_distancing
      , _26_ AS which_groups_regularly_teach_classes_to_students_in_current_school_year
      , yesNoToInt(_27_) AS are_there_any_children_in_the_household_that_did_not_advance_to_next_grade_from_last_school_year
      , _28_ AS what_is_the_agreement_with_statement_children_advanced_grade_without_meeting_requirements_from_last_school_year
      , _29_ AS what_items_were_requested_at_the_time_of_enrollment_in_the_current_school_year
      , _30_ AS what_is_the_level_of_knowledge_of_biosecurity_measures_implemented_in_current_school_year
      , _31_ AS what_is_the_condition_of_the_school_physical_structure
      , _32_ AS how_often_do_the_schools_have_a_potable_water_supply
      , yesNoToInt(_33_) AS do_the_schools_have_functioning_bathrooms
      , _34_ AS how_often_do_the_schools_have_electricity
      , _35_ AS do_the_schools_provide_sufficient_antibacterial_gel_for_students
      , _36_ AS do_the_schools_provide_sufficient_masks_for_students
      , _37_ AS how_many_teachers_are_vaccinated_in_current_school_year
      , _38_ AS how_many_children_in_the_classroom_are_vaccinated_in_current_school_year
      , _39_ AS how_many_people_who_live_with_the_children_in_the_household_are_vaccinated
  FROM source_table AS lt
  LEFT JOIN vzla_loc vl ON lt.submission_id = vl.submission_id
)
SELECT *
FROM location_enriched
