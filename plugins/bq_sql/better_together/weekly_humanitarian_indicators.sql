WITH WEEKLY_HUMANITARIAN_INDICATORS AS (
-- compute the sum of the results of the question for each row
SELECT
    DATE_TRUNC(submission_date, WEEK(MONDAY)) as submission_week,
    gender,
    age,
    `geography`,
    financial_situation,
    education,
    employment_status,
    submission_state,
    submission_municipality,
    submission_parish,
    cereals_plantains_tubers_7days * 2 + grains_7days * 3 + vegs_7days +
        fruits_7days + meat_eggs_seafood_7days * 4 + dairy_7days * 4 +
         oils_fats_7days * 0.5 + sugars_7days * 0.5 as fcs
FROM `{{ params.src_table }}` )
-- run the aggregates on the result
SELECT
    submission_week,
    gender,
    age,
    `geography`,
    financial_situation,
    education,
    employment_status,
    submission_state,
    submission_municipality,
    submission_parish,
    SUM(fcs) AS fcs_sum,
    COUNT(fcs) AS population_count
FROM WEEKLY_HUMANITARIAN_INDICATORS
GROUP BY
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10
