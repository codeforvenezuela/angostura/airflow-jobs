WITH src_table AS (
    SELECT * FROM `{{ params.src_table }}`
), raw_union_agg AS (
    SELECT submission_id, form_id
        , 1 AS question_order
        , _1_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 2 AS question_order
        , _2_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 3 AS question_order
        , _3_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 4 AS question_order
        , _4_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 5 AS question_order
        , _5_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 6 AS question_order
        , _6_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 7 AS question_order
        , _7_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 8 AS question_order
        , _8_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 9 AS question_order
        , _9_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 10 AS question_order
        , _10_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 11 AS question_order
        , _11_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 12 AS question_order
        , _12_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 13 AS question_order
        , _13_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 14 AS question_order
        , _14_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 15 AS question_order
        , _15_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 16 AS question_order
        , _16_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 17 AS question_order
        , _17_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 18 AS question_order
        , _18_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 19 AS question_order
        , _19_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 20 AS question_order
        , _20_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 21 AS question_order
        , _21_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 22 AS question_order
        , _22_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 23 AS question_order
        , _23_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 24 AS question_order
        , _24_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 25 AS question_order
        , _25_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 26 AS question_order
        , _26_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 27 AS question_order
        , _27_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 28 AS question_order
        , _28_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 29 AS question_order
        , _29_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 30 AS question_order
        , _30_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 31 AS question_order
        , _31_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 32 AS question_order
        , _32_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 33 AS question_order
        , _33_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 34 AS question_order
        , _34_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 35 AS question_order
        , _35_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 36 AS question_order
        , _36_ AS response_label
    FROM src_table
    UNION ALL
    SELECT submission_id, form_id
        , 37 AS question_order
        , _37_ AS response_label
    FROM src_table
), -- generate a row for each selected options available in response_label
   -- note: null responses are filtered at this point and that's ok
  raw_unnest_agg AS (
    SELECT submission_id, form_id, question_order, res AS response_label
    FROM raw_union_agg, UNNEST(SPLIT(response_label, '^')) res
), -- move forward with responses for columns that existed at the time
  unnest_agg AS (
    SELECT *
    FROM raw_unnest_agg
    WHERE response_label != 'did not exist'
), latest_survey_data AS (
    SELECT form_id
        , form_version
        , language
        , question_type
        , question_order
        , question_value
        , question_label
        , response_order
        , response_value
        , response_label
    FROM `event-pipeline.better_together_raw.survey_questions_metadata`
    WHERE latest_form_version
), -- extract question and response order from fixed answers in English
  fixed_en AS (
    SELECT t.submission_id, md.form_id, md.question_order, md.response_order
    FROM unnest_agg t
    JOIN latest_survey_data AS md
      USING (form_id, question_order, response_label)
    WHERE md.question_type IN ('SELECT_ONE', 'SELECT_MANY') AND language = 'EN'
), -- translations for fixed answers can be found by joining back
  fixed_translated AS (
    SELECT t.submission_id, md.*
    FROM fixed_en t
    JOIN latest_survey_data AS md
      USING (form_id, question_order, response_order)
),-- text and numbers answers should be joined by (question_order) only
  free_answers AS (
    SELECT t.submission_id, md.*
    FROM unnest_agg t
    JOIN latest_survey_data AS md
      USING (form_id, question_order)
    WHERE md.question_type IN ('TEXT', 'NUMBER')
), long_table AS (
    SELECT * FROM fixed_translated
    UNION ALL
    SELECT * FROM free_answers
)
SELECT * FROM long_table
