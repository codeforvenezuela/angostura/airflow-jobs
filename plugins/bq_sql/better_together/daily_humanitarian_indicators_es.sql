WITH src_table AS (
    SELECT * FROM `{{ params.src_table }}`
), premise_translations AS (
  SELECT *
  FROM `event-pipeline.better_together_raw.premise_contributor_profile`
)
SELECT submission_date
  , submission_state
  , submission_municipality
  , submission_parish
  , hdds_sum
  , hdds_high_count
  , hdds_medium_count
  , hdds_low_count
  , fcs_sum
  , group_count
  , gender_j.inputs_es AS gender
  , age_j.inputs_es as age
  , geography_j.inputs_es as geography
  , financial_j.inputs_es as financial_situation
  , education_j.inputs_es as education
  , employment_status_j.inputs_es as employment_status
FROM src_table AS src
LEFT JOIN premise_translations gender_j ON gender_j.inputs_en = src.gender
  AND gender_j.column='gender'
LEFT JOIN premise_translations age_j ON age_j.inputs_en = src.age
  AND age_j.column='age'
LEFT JOIN premise_translations geography_j ON geography_j.inputs_en = src.geography
  AND geography_j.column='geography'
LEFT JOIN premise_translations financial_j ON financial_j.inputs_en = src.financial_situation
  AND financial_j.column='financial_situation'
LEFT JOIN premise_translations education_j ON education_j.inputs_en = src.education
  AND education_j.column='education'
LEFT JOIN premise_translations employment_status_j ON employment_status_j.inputs_en = src.employment_status
  AND employment_status_j.column='employment_status'
