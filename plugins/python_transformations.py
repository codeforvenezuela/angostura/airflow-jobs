"""
Functions imported by the `BigQueryPythonOperator` to run python tasks.

For a reference to write your own transformation, see this link:

https://www.notion.so/codeforvenezuela/Generic-dags-configuration-db7276c96d8047e987fae7caa1006ab0
"""

import pandas as pd


def test_function(probe):
    """Test function."""
    data = {
        "metric": ["Count", "Sum"],
        "value": [probe.count()["timestamp"], probe.sum()["timestamp"]],
    }
    return pd.DataFrame(data)


def bt_long_to_wide_simple(long_table):
    """
    Takes a pd.DataFrame containing a (whole) long table in the better-together schema,
    and outputs a wide simple table that can be later-joined with other common
    fields.
    """
    wide = pd.pivot(
        long_table, index="submission_id", columns="question_order", values="response_label"
    )
    wide["submission_id"] = wide.index
    return wide
